package net.i2p.util;

/*
 * Free software
 */

/**
 * 
 * @param <E> the type of elements held in this collection
 */
public class LockFreeCache<E> {

    final int minCapacity;
    final int maxCapacity;

    public static interface ObjectFactory<E> {
        E newInstance();
    }
    
    private final ObjectFactory<E> factory;

    private final ThreadStore<LockFreeRingBuffer<E>> cache = new ThreadStore<LockFreeRingBuffer<E>>(new BufferFactory());

    public LockFreeCache(ObjectFactory<E> factory, int minCapacity, int maxCapacity) {
        this.factory = factory;
        this.maxCapacity = maxCapacity;
        this.minCapacity = minCapacity;
    }

    public LockFreeCache(ObjectFactory<E> factory, int capacity) {
        this(factory, capacity, capacity);
    }
    
    private class BufferFactory implements ThreadStore.ObjectFactory<LockFreeRingBuffer<E>> {
        public LockFreeRingBuffer<E> newInstance() {
            return new LockFreeRingBuffer<E>(minCapacity, maxCapacity);
        }
    }

    public boolean offer(E e) {
        return cache.get().offer(e);
    }

    public E poll() {
        E rv = cache.get().poll();
        if (rv == null)
            rv = factory.newInstance();
        return rv;
    }

    public void clear() {
        for (Object r : cache.values())
            ((LockFreeRingBuffer<E>)r).clear();
    }

    // public E pollWithHint() {
    //     Buffer b = cache.get();
    //     if (b.size() == 0)
    //         return cache.getHint().poll();
    //     return b.poll();
    // }
}