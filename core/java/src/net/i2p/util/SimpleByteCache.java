package net.i2p.util;

/**
 * Like ByteCache but works directly with byte arrays, not ByteArrays.
 * These are designed to be small caches, so there's no cleaner task
 * like there is in ByteCache. And we don't zero out the arrays here.
 * Only the static methods are public here.
 *
 * @since 0.8.3
 */
public final class SimpleByteCache {

    private static final int MAX_LEN = 257;
    private static final int DEFAULT_SIZE = 256;

    private static final SimpleByteCache[] _caches = new SimpleByteCache[MAX_LEN + 1];

    /**
     * Get a cache responsible for arrays of the given size
     *
     * @param size how large should the objects cached be?
     */
    public static SimpleByteCache getInstance(int size) {
        if (size <= 0 || size > MAX_LEN)
            return null;
        SimpleByteCache cache = _caches[size];
        if (cache == null)
            synchronized (_caches) {
                cache = _caches[size];
                if (cache == null) {
                    cache = new SimpleByteCache(DEFAULT_SIZE, size);
                    _caches[size] = cache;
                }
        }
        return cache;
    }

    /**
     *  Clear everything (memory pressure)
     */
    public static void clearAll() {
        for (int i = 1; i < MAX_LEN; i++) {
            SimpleByteCache cache = _caches[i];
            if (cache != null)
                cache.clear();
        }
    }

    private final LockFreeCache<byte[]> _available;
    private final int _entrySize;
    
    /** @since 0.9.36 */
    private static class ByteArrayFactory implements LockFreeCache.ObjectFactory<byte[]> {
        private final int sz;

        ByteArrayFactory(int entrySize) {
            sz = entrySize;
        }

        public byte[] newInstance() {
            return new byte[sz];
        }
    }

    private SimpleByteCache(int maxCachedEntries, int entrySize) {
        _available = new LockFreeCache<byte[]>(new ByteArrayFactory(entrySize), maxCachedEntries);
        _entrySize = entrySize;
    }
    
    /**
     * Get the next available array, either from the cache or a brand new one
     */
    public static byte[] acquire(int size) {
        return getInstance(size).acquire();
    }

     /**
     * Get the next available array, either from the cache or a brand new one
     */
    public byte[] acquire() {
        return _available.poll();
    }
    
    /**
     * Put this array back onto the available cache for reuse
     */
    public static void release(byte[] entry) {
        if (entry == null || entry.length > MAX_LEN)
            return;
        SimpleByteCache cache = _caches[entry.length];
        if (cache != null)
            cache.releaseIt(entry);
    }

    /**
     * Put this array back onto the available cache for reuse
     */
    public void releaseIt(byte[] entry) {
        if (entry.length != _entrySize)
            return;
        // should be safe without this
        //Arrays.fill(entry, (byte) 0);
        _available.offer(entry);
    }
    
    /**
     *  Clear everything (memory pressure)
     */
    private void clear() {
        _available.clear();
    }
}
