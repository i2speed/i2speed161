package net.i2p.util;

import java.util.Collection;
import java.util.List;

/*
 * Free software
 */

import java.util.concurrent.atomic.*;


/**
 * This implements part of the queue interface and
 * replaces concurrent queues as well as hashsets when used to hand over items from thread A to B
 * should be sized to hold at least 200 ms of traffic due to effects described below
 * in case of a race poll() will not wait for offer() to complete
 * minimum 16 items enforced
 * 
 * @param <E> the type of elements held in this collection
 */
public class LockFreeQueue<E>{

    private final int maxcapacity;
    private final AtomicInteger size = new AtomicInteger(0);
    private boolean taking = false;
    private boolean idle;
    private final ThreadStore<LockFreeRingBuffer<E>> map = new ThreadStore<LockFreeRingBuffer<E>>(new BufferFactory());

    public LockFreeQueue(int maxcapacity) {
        this.maxcapacity = maxcapacity;
    }

    private class BufferFactory implements ThreadStore.ObjectFactory<LockFreeRingBuffer<E>> {
        public LockFreeRingBuffer<E> newInstance() {
            // suppose we will have at least 2 producers
            return new LockFreeRingBuffer<E>(maxcapacity >> 1);
        }
    }

    public boolean isEmpty() {
        return size.get() <= 0; // do cache update
    }

    public int size() {
        int s = size.get(); // do cache update
        return s <= 0 ? 0 : s;
    }

    public int getCapacity() {
        return maxcapacity;
    }

    public boolean isBacklogged() {
        return size() > maxcapacity << 1;
    }

    private void wakeup() {
        if (taking) {
            synchronized (map) {
                idle = false;
                map.notify();
            }
        }
    }

    public boolean offer(E e) {
        if (e == null) return true;
        boolean rv = map.get().offer(e);
        if (rv && size.getAndIncrement() <= 0)
            wakeup();
        return rv;
    }

    public void drainTo(LockFreeQueue<E> c) {
        E e;
        while ((e = threaded_poll(true)) != null)
            c.offer(e);
    }

    public void drainTo(List<E> c) {
        E e;
        while ((e = threaded_poll(true)) != null)
            c.add(e);
    }

    /**
     * alternative to addAll() not requiring an iterator
     * 
     * @param c Collection to be filled
     */
    public void populate(Collection c) {
        // best effort
        for (Object r : map.values())
            ((LockFreeRingBuffer<E>)r).populate(c);
    }

    public E poll() {
        if (size.get() <= 0) // do cache update
            return null;
        E rv = null;
        try {
            LockFreeRingBuffer<E> hint = map.getHint();
            rv = hint.poll();
            if (rv != null) return rv;
            for (Object r : map.values()) {
                if (r != hint) {
                    rv = ((LockFreeRingBuffer<E>)r).poll();
                    if (rv != null) break;
                }
            }
            if (rv == null) {
                System.out.println("oops: We have " + size.get());
                try {
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if (rv != null) size.decrementAndGet(); // do cache update
        }
        return rv;
    }   

    public E threaded_poll(boolean force) {
        if (size.get() == 0) return null;
        if (size.decrementAndGet() < 0) { // rare possible race for the last entry
            size.incrementAndGet();
            return null;
        }
        do {
            LockFreeRingBuffer<E> hint = map.getHint();
            E rv = hint.locked_poll();
            if (rv != null) return rv;
            for (Object r : map.values()) {
                if (r != hint) {
                    rv = ((LockFreeRingBuffer<E>)r).locked_poll();
                    if (rv != null) return rv;
                }
            }
        } while (force);
        size.incrementAndGet();
        return null;
    }

    public E peek() {
        if (size.get() == 0) // do cache update
            return null;
        E rv = map.getHint().peek();
        if (rv == null) {
            for (Object r : map.values()) {
                rv = ((LockFreeRingBuffer<E>)r).peek();
                if (rv != null) break;
            }
        }
        return rv;
    }

    public E take() {
        // do not let the evil OSes suspend us too long
        return take(50);
    }

    public E threaded_take() {
        // do not let the evil OSes suspend us too long
        return threaded_take(50);
    }

    public E take(long timeOut) {
        idle = true;
        E rv;
        if ((rv = poll()) != null) return rv;
        taking = true;
        synchronized (map) {
            try {
                if (idle) map.wait(timeOut);
            } catch (InterruptedException ie) {};
        }
        return poll();
    }

    public E threaded_take(long timeOut) {
        idle = true;
        E rv;
        if ((rv = threaded_poll(true)) != null) return rv;
        taking = true;
        synchronized (map) {
            try {
                if (idle) map.wait(timeOut);
            } catch (InterruptedException ie) {};
        }
        return threaded_poll(false);
    }

    public void clear() {
        while (threaded_poll(true) != null);
    }

    // call this from the consumer thread
    public void abort() {
        clear();
        synchronized (map) {
            idle = false;
            map.notify();
        }
    }
}