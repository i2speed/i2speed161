package net.i2p.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Collection;

/**
 *  A concurrent map for infrequent updates
 */
public class CopyOnWriteMap<K, V> {
    private volatile Map<K, V> map;
    private final int _max;


    public CopyOnWriteMap(int max) {
        _max = max;
        map = new HashMap<K, V>(_max);
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public int size() {
        return map.size();
    }

    public boolean containsKey(K key) {
        return map.containsKey(key);
    }

    public boolean containsValue(V value) {
        return map.containsValue(value);
    }

    public synchronized void clear() {
        map = new HashMap<K, V>(_max);
    }

    public V get(K key) {
        return map.get(key);
    }

    public Map<K, V> map() {
        return map;
    }

    public synchronized V put(K key, V value) {
        Map<K, V> map2 = map;
        V rv = map2.get(key);
        if (rv != value) {
            map2 = new HashMap<K, V>(map2);
            rv = map2.put(key, value);
            map = map2;
        }
        return rv;
    }

    public synchronized void putAll(Map<K, V> C) {
        if (C.isEmpty()) return;
        Map<K, V> newmap = new HashMap<K, V>(map);
        newmap.putAll(C);
        map = newmap;
    }

    public synchronized V remove(K key) {
        Map<K, V> map2 = map;
        V rv = map2.get(key);
        if (rv == null) return null;
        if (map2.size() > 1) {
            map2 = new HashMap<K, V>(map2);
            rv = map2.remove(key);
            map = map2;
        } else
            clear();
        return rv;
    }

    public synchronized V putIfAbsent(K key, V value) {
        Map<K, V> map2 = map;
        V rv = map2.get(key);
        if (rv == null) {
            map2 = new HashMap<K, V>(map2);
            rv = map2.put(key, value);
            map = map2;
        }
        return rv;
    }

    public Collection<V> values() {
        return map.values();
    }
/*
    public Iterator<K, V> iterator() {
        return map.get().iterator();
    }
*/

}
