package net.i2p.util;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An object cache which is safe to use by multiple threads without blocking.
 * 
 * @author zab
 *
 * @param <T>
 * @since 0.9.36
 */
public class TryCache<T> {

    /**
     * Something that creates objects of the type cached by this cache
     *
     * @param <T>
     */
    public static interface ObjectFactory<T> {
        T newInstance();
    }
    
    private final ObjectFactory<T> factory;
    protected final int capacity;
    protected final Queue<T> items;
    protected final Lock lock = new ReentrantLock();
    protected long _lastUnderflow;

    /**
     * @param factory to be used for creating new instances
     * @param capacity cache up to this many items
     */
    public TryCache(ObjectFactory<T> factory, int capacity) {
        this.factory = factory;
        this.capacity = capacity;
        this.items = new ArrayDeque<T>(capacity);
    }
    
    /**
     * @return a cached or newly created item from this cache
     */
    public T acquire() {
        T rv = null;
        if (items.size() > 0 && lock.tryLock()) {
            try {
                rv = items.poll();
            } finally {
                lock.unlock();
            }
        }

        if (rv == null) {
            rv = factory.newInstance();
            _lastUnderflow = System.currentTimeMillis();
        }

        return rv;
    }
    
    /**
     * Tries to return this item to the cache but it may fail if
     * the cache has reached capacity or it's lock is held by
     * another thread.
     */
    public void release(T item) {
        if (items.size() < capacity)
            if (lock.tryLock()) {
                try {
                    items.offer(item);
                } finally {
                    lock.unlock();
                }
            }
    }
    
    /**
     * Clears all cached items.  This is the only method
     * that blocks until it acquires the lock.
     */
    public void clear() {
        lock.lock();
        try {
            items.clear();
        } finally {
            lock.unlock();
        }
    }
}
