package net.i2p.util;

import java.util.Arrays;

/*
 * Free software
 */

/**
 * 
 * @param <E> the type of elements held in this collection
 */
public class ThreadStore<E>{

    public static interface ObjectFactory<E> {
        E newInstance();
    }
    
    private final ObjectFactory<E> factory;

    ThreadLocal<E> threadLocalValue = new ThreadLocal<>();

    private Object[] map;

    private E hint;

    public ThreadStore(ObjectFactory<E> factory) {
        this.factory = factory;
        map = new Object[0];
    }

    public E get() {
        E rv = threadLocalValue.get();
        if (rv == null) {
            threadLocalValue.set(rv = factory.newInstance());
            synchronized (this) {
                int len = map.length;
                Object[] newmap = Arrays.copyOf(map, 1 + len);
                newmap[len] = hint = rv;
                map = newmap;
            }
        } else hint = rv;
        return rv;
    }

    public E getHint() {
        return hint;
    }

    public Object[] values() {
        return map;
    }
}