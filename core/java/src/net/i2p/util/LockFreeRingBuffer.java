package net.i2p.util;

import java.util.Arrays;
import java.util.Collection;

/*
 * Free software
 */

import java.util.concurrent.atomic.*;

/**
 * This implements part of the queue interface and
 * replaces concurrent queues as well as hashsets when used to hand over items from thread A to B
 * should be sized to hold at least 200 ms of traffic due to effects described below
 * in case of a race poll() will not wait for offer() to complete
 * minimum 16 items enforced
 * 
 * @param <E> the type of elements held in this collection
 */
public class LockFreeRingBuffer<E>{

    static final int MINBUFFER = 16;

    private int ceilingNextPowerOfTwo(int x) {
        // From Hacker's Delight, Chapter 3, Harry S. Warren Jr.
        return 1 << (Integer.SIZE - Integer.numberOfLeadingZeros(x - 1));
    }

    private Object[] array;
    /* head pointing at first field valid */
    private int head;
    /* tail pointing past last field valid */
    private int tail;
    private AtomicInteger size = new AtomicInteger();
    private final int minCapacity;
    private final int maxCapacity;
    private final AtomicBoolean lock = new AtomicBoolean();
    private boolean taking = false;
    private boolean idle;

    public LockFreeRingBuffer(int minCapacity, int maxCapacity) {
        if (minCapacity < MINBUFFER) minCapacity = MINBUFFER;
        if (maxCapacity < minCapacity) maxCapacity = minCapacity;
        if (minCapacity > 99999) minCapacity = 99999;
        if (maxCapacity > 99999) maxCapacity = 99999;
        this.maxCapacity = ceilingNextPowerOfTwo(maxCapacity);
        this.minCapacity = ceilingNextPowerOfTwo(minCapacity);
        array = new Object[this.minCapacity];
    }

    public LockFreeRingBuffer(int capacity) {
        this(MINBUFFER, capacity);
    }

    public boolean isEmpty() {
        return size() <= 0;
    }

    public int size() {
        return size.get();
    }

    public int getCapacity() {
        return maxCapacity;
    }

    public boolean isBacklogged() {
        // do not bother syncing
        return tail - head > maxCapacity << 1;
    }

    // not thread-safe
    public void rotate() {
        if (tail - head <= 1) return;
        int mask = array.length - 1;
        array[tail++ & mask] = array[head++ & mask];
    }

    public boolean offer(E e) {
        if (e == null) return true;
        int s = size.get();
        if (s == 0) {
            if (array.length > minCapacity)
                array = new Object[minCapacity];
            array[head = 0] = e;
            size.set(tail = 1);
            if (taking) synchronized (this) {
                idle = false;
                this.notify();
            }
            return true;
        }
        if (s == array.length) {
            if (s == maxCapacity) return false; // full
            int newsize = s << 1;
            Object[] newarray = Arrays.copyOf(array, newsize);
            System.arraycopy(newarray, 0, newarray, s, s);
            newarray[tail & (newsize - 1)] = e;
            array = newarray;
        } else
            array[tail & (array.length - 1)] = e;
        tail++;
        size.incrementAndGet();
        return true;
    }

    public E peek() {
        if (size.get() == 0)
            return null;
        Object[] work = array;
        return (E)work[head & (work.length - 1)];
    }

    /**
     * alternative to addAll() not requiring an iterator
     * 
     * @param c Collection to be filled
     */
    public void populate(Collection c) {
        // best effort
        int loops = size.get(); // do cache update
        Object[] work = array;
        int mask = work.length - 1;
        for (int index = head; loops > 0; loops--) {
            E o = (E)work[index++ & mask];
            if (o != null) c.add(o); // there might have been a race
        }
    }

    public void drainTo(LockFreeRingBuffer<E> c) {
        E e;
        while ((e = poll()) != null)
            c.offer(e);
    }

    public boolean contains(E o) {
        // best effort
        int loops = size.get(); // do cache update
        Object[] work = array;
        int mask = work.length - 1;
        for (int index = head; loops > 0; loops--)
            if (o == work[index++ & mask])
                return true;
        return false;
    }

    public void pop() { // single consumer only after successful peek()
        head++;
        size.decrementAndGet();
    }

    public E locked_poll() {
        if (size.get() == 0 || !lock.compareAndSet(false, true)) return null;
        E rv = size.get() == 0 ? null : sure_poll(); // watch for race
        lock.set(false);
        return rv;
    }

    private E sure_poll() {
        Object[] work = array;
        int h = head++ & (work.length - 1);
        E rv = (E)work[h];
        if (rv == null) {
            System.out.println("Null poll: " + size.get() + "  head " + head + "  tail " + tail);
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        work[h] = null;
        size.decrementAndGet();
        return rv;
    }

    public E poll() {
        if (size.get() == 0)
            return null;
        return sure_poll();
    }

    // not thread-safe
    public E take() {
        // do not let the evil OSes suspend us too long
        return take(50);
    }

    // not thread-safe
    public E take(long timeOut) {
        idle = true;
        E rv;
        if ((rv = poll()) != null) return rv;
        taking = true;
        synchronized (this) {
            try {
                if (idle) this.wait(timeOut);
            } catch (InterruptedException ie) {};
        }
        return poll();
    }

    public void clear() {
        while (poll() != null);
    }

    // not thread-safe
    public void abort() {
        clear();
        synchronized (this) {
            idle = false;
            this.notify();
        }
    }
}