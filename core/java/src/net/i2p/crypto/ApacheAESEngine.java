package net.i2p.crypto;

/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain 
 * with no warranty of any kind, either expressed or implied.  
 * It probably won't  make your computer catch on fire, or eat 
 * your children, but it might.  Use at your own risk.
 *
 */

import java.util.Properties;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.crypto.cipher.CryptoCipher;
import org.apache.commons.crypto.cipher.CryptoCipherFactory;
import org.apache.commons.crypto.cipher.CryptoCipherFactory.CipherProvider;
import org.apache.commons.crypto.utils.Utils;

import net.i2p.I2PAppContext;
import net.i2p.data.SessionKey;
import net.i2p.util.ThreadStore;
import net.i2p.util.Log;

/** 
 * Wrapper for AES cypher operation using Apache commons crypto.  Implements
 * CBC with a 16 byte IV.
 * Problems: 
 * Only supports data of size mod 16 bytes - no inherent padding.
 *
 */
public final class ApacheAESEngine extends AESEngine {
    private static final ThreadStore<CryptoCipher> _ciphers;
    private static final Properties properties = new Properties();
    static {
        properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CipherProvider.OPENSSL.getClassName());
        _ciphers = new ThreadStore<CryptoCipher>(new BufferFactory());
    }

    public ApacheAESEngine(I2PAppContext context) {
        super(context);
    }
    
    /**
     *  @param iv must be 16 bytes
     *  @param length must be a multiple of 16
     */
    @Override
    public void encrypt(byte payload[], int payloadIndex, byte out[], int outIndex, SessionKey sessionKey, byte iv[], int length) {
        encrypt(payload, payloadIndex, out, outIndex, sessionKey, iv, 0, length);
    }

    /**
     *  @param iv must be 16 bytes
     *  @param length must be a multiple of 16
     */
    @Override
    public void encrypt(byte payload[], int payloadIndex, byte out[], int outIndex, SessionKey sessionKey, byte iv[], int ivOffset, int length) {
        if (length > 160) { // cipher.init is inefficient for small chunks
            // but microoptimization for 2% case
            try {
                SecretKeySpec key1 = new SecretKeySpec(sessionKey.getData(), "AES");
                IvParameterSpec ivps;
                ivps = new IvParameterSpec(iv, ivOffset, 16);
                CryptoCipher cipher = acquire();
                cipher.init(Cipher.ENCRYPT_MODE, key1, ivps);
                cipher.doFinal(payload, payloadIndex, length, out, outIndex);
                // release(cipher);
            } catch (GeneralSecurityException gse) {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Apache AES encrypt fail", gse);
            }
            return;
        }
        CryptixAESKeyCache.KeyCacheEntry key;
        try {
            key = (CryptixAESKeyCache.KeyCacheEntry)(sessionKey.getPreparedKey());
        } catch (InvalidKeyException ike) {
            _log.log(Log.CRIT, "Invalid key", ike);
            throw new IllegalArgumentException("invalid key?  " + ike.getMessage());
        }
        AESContainer buffer = new AESContainer();
        buffer.fill(iv, ivOffset);
        for (length = length >> 4; length > 0; length--) {
            CryptixRijndael_Algorithm.blockEncryptFast(buffer, payload, payloadIndex, key);
            buffer.dump(out, outIndex);
            payloadIndex += 16;
            outIndex += 16;
        }
    }
    
    /**
     *  @param iv 16 bytes
     *  @param length must be a multiple of 16 (will overrun to next mod 16 if not)
     */
    @Override
    public void decrypt(byte payload[], int payloadIndex, byte out[], int outIndex, SessionKey sessionKey, byte iv[], int length) {
        decrypt(payload, payloadIndex, out, outIndex, sessionKey, iv, 0, length);
    }

    /**
     *  @param iv 16 bytes starting at ivOffset
     *  @param length must be a multiple of 16 (will overrun to next mod 16 if not)
     */
    @Override
    public void decrypt(byte payload[], int payloadIndex, byte out[], int outIndex, SessionKey sessionKey, byte iv[], int ivOffset, int length) {
        if (length > 160) {
            try {
                SecretKeySpec key = new SecretKeySpec(sessionKey.getData(), "AES");
                IvParameterSpec ivps;
                ivps = new IvParameterSpec(iv, ivOffset, 16);
                CryptoCipher cipher = acquire();
                cipher.init(Cipher.DECRYPT_MODE, key, ivps);
                cipher.doFinal(payload, payloadIndex, length, out, outIndex);
                // release(cipher);
                return;
            } catch (GeneralSecurityException gse) {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Apache AES decrypt fail", gse);
            }
            return;
        }

        int numblock = length >> 4;
        if (length > numblock << 4) {
            // may not work, it will overrun payload length and could AIOOBE
            numblock++;
            if (_log.shouldLog(Log.WARN))
                _log.warn("not %16 " + length, new Exception());
        }
        CryptixAESKeyCache.KeyCacheEntry key;
        try {
            key = ((CryptixAESKeyCache.KeyCacheEntry)(sessionKey.getPreparedKey()));
        } catch (InvalidKeyException ike) {
            _log.log(Log.CRIT, "Invalid key", ike);
            throw new IllegalArgumentException("invalid key?  " + ike.getMessage());
        }
        // payload and out may be identical!
        if (out == payload)
            payload = payload.clone();
        AESContainer buffer = new AESContainer();
        for (int x = numblock; x > 0; x--) {
            buffer.fill(iv, ivOffset);
            CryptixRijndael_Algorithm.blockDecryptFast(buffer, payload, payloadIndex, key);
            //DataHelper.xor(out, outIndex + x * 16, prev, 0, out, outIndex + x * 16, 16);
            buffer.dump(out, outIndex);
            iv = payload;
            ivOffset = payloadIndex;
            payloadIndex += 16;
            outIndex += 16;
        }        

    }        
    
    /** encrypt exactly 16 bytes using the session key
     * @param payload plaintext data, 16 bytes starting at inIndex
     * @param sessionKey private session key
     * @param out out parameter, 16 bytes starting at outIndex
     */
    @Override
    public final void encryptBlock(byte payload[], int inIndex, SessionKey sessionKey, byte out[], int outIndex) {
        Object pkey;
        try {
            pkey = sessionKey.getPreparedKey();
        } catch (InvalidKeyException ike) {
            _log.log(Log.CRIT, "Invalid key", ike);
            throw new IllegalArgumentException("invalid key?  " + ike.getMessage());
        }
        CryptixRijndael_Algorithm.blockEncrypt(payload, out, inIndex, outIndex, pkey);
    }

    /** decrypt exactly 16 bytes of data with the session key provided
     * @param payload encrypted data, 16 bytes starting at inIndex
     * @param sessionKey private session key
     * @param rv out parameter, 16 bytes starting at outIndex
     */
    @Override
    public final void decryptBlock(byte payload[], int inIndex, SessionKey sessionKey, byte rv[], int outIndex) {
        Object pkey;
        try {
            pkey = sessionKey.getPreparedKey();
        } catch (InvalidKeyException ike) {
            _log.log(Log.CRIT, "Invalid key", ike);
            throw new IllegalArgumentException("invalid key?  " + ike.getMessage());
        }
        CryptixRijndael_Algorithm.blockDecrypt(payload, rv, inIndex, outIndex, pkey);
    }
    
    /**
     *  @return cached or new
     *  @since 0.9.49
     */
    private CryptoCipher acquire() {
        return _ciphers.get();
    }
    
    private static class BufferFactory implements ThreadStore.ObjectFactory<CryptoCipher> {
        public CryptoCipher newInstance() {
            CryptoCipher rv;
            try {
                rv = Utils.getCipherInstance("AES/CBC/NoPadding", properties);
            } catch (IOException e) {
                throw new UnsupportedOperationException("AES/CBC/NoPadding", e);
            }
            return rv;
        }
    }

    /**
     */
    private void release(CryptoCipher cipher) {
    }
}
