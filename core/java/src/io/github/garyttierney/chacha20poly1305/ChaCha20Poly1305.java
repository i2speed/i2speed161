package io.github.garyttierney.chacha20poly1305;

import java.nio.ByteBuffer;

public final class ChaCha20Poly1305 {

	public static final int KEYBYTES = 32;
	public static final int NPUBBYTES = 8;
	public static final int ABYTES = 16;

	public static native int decrypt(ByteBuffer keyBuffer, ByteBuffer ciphertext,
							  int ciphertextLength, ByteBuffer additionalData,
							  int additionalDataLength, ByteBuffer nonce,
							  ByteBuffer target);

	public static native int encrypt(ByteBuffer keyBuffer, ByteBuffer plaintext,
							  int plaintextLength, ByteBuffer additionalData,
							  int additionalDataLength, ByteBuffer nonce,
							  ByteBuffer target);
}
