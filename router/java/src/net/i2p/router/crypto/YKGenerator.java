package net.i2p.router.crypto;

/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain
 * with no warranty of any kind, either expressed or implied.
 * It probably won't  make your computer catch on fire, or eat
 * your children, but it might.  Use at your own risk.
 *
 */

import java.math.BigInteger;
import java.util.concurrent.atomic.*;

import net.i2p.router.RouterContext;
import net.i2p.util.LockFreeQueue;
import net.i2p.util.LockFreeRingBuffer;
import net.i2p.util.NativeBigInteger;
import net.i2p.util.SystemVersion;
import net.i2p.crypto.CryptoConstants;
import net.i2p.router.Job;
import net.i2p.router.JobImpl;

/**
 * Precalculate the Y and K for ElGamal encryption operations.
 *
 * This class precalcs a set of values on its own thread, using those transparently
 * when a new instance is created.  By default, the minimum threshold for creating
 * new values for the pool is 20, and the max pool size is 50.  Whenever the pool has
 * less than the minimum, it fills it up again to the max.  There is a delay after
 * each precalculation so that the CPU isn't hosed during startup.
 * These three parameters are controlled by java environmental variables and
 * can be adjusted via:
 *  -Dcrypto.yk.precalc.min=40 -Dcrypto.yk.precalc.max=100 -Dcrypto.yk.precalc.delay=60000
 *
 * (delay is milliseconds)
 *
 * To disable precalculation, set min to 0
 *
 * @author jrandom
 */
final class YKGenerator {
    //private final static Log _log = new Log(YKGenerator.class);
    private int MIN_NUM_BUILDERS = 0;
    private int MAX_NUM_BUILDERS = 1;
    private final LockFreeQueue<BigInteger[]> _values;
    private final RouterContext ctx;
    private boolean _isRunning;

    private AtomicBoolean _jobRunning = new AtomicBoolean();
    private Job _job = new YKPrecalcJob();

    /**
     *  Caller must also call start() to start the background precalc thread.
     *  Unit tests will still work without calling start().
     */
    public YKGenerator(RouterContext context) {
        ctx = context;

        // add to the defaults for every 128MB of RAM, up to 1GB

        _values = new LockFreeQueue<BigInteger[]>(999);

        //if (_log.shouldLog(Log.DEBUG))
        //    _log.debug("ElGamal YK Precalc (minimum: " + MIN_NUM_BUILDERS + " max: " + MAX_NUM_BUILDERS + ", delay: "
        //               + CALC_DELAY + ")");

        ctx.statManager().createRateStat("crypto.YKUsed", "Need a YK from the queue", "Encryption", new long[] { 60*60*1000 });
        ctx.statManager().createRateStat("crypto.YKEmpty", "YK queue empty", "Encryption", new long[] { 60*60*1000 });
    }

    /**
     *  Start the background precalc thread.
     *  Must be called for normal operation.
     *  If not called, all generation happens in the foreground.
     *  Not required for unit tests.
     *
     *  @since 0.9.14
     */
    public synchronized void start() {
        _isRunning = true;
    }

    /**
     *  Stop the background precalc thread.
     *  Can be restarted.
     *  Not required for unit tests.
     *
     *  @since 0.8.8
     */
    public synchronized void shutdown() {
        _isRunning = false;
        _values.clear();
    }

    /** @return true if successful, false if full */
    private final boolean addValues(BigInteger yk[]) {
        return _values.offer(yk);
    }

    /** @return rv[0] = Y; rv[1] = K */
    public BigInteger[] getNextYK() {
        if (_values.size() <= MIN_NUM_BUILDERS && _isRunning && _jobRunning.compareAndSet(false, true))
            ctx.jobQueue().prioJob(_job);
        ctx.statManager().addRateData("crypto.YKUsed", 1);
        BigInteger[] rv = _values.threaded_poll(false);
        if (rv == null) {
            rv = generateYK();
            ctx.statManager().addRateData("crypto.YKEmpty", 1);
        }
        return rv;
    }

    private final static BigInteger _two = new NativeBigInteger(1, new byte[] { 0x02});

    /** @return rv[0] = Y; rv[1] = K */
    private final BigInteger[] generateYK() {
        NativeBigInteger k = null;
        BigInteger y = null;
        //long t0 = 0;
        //long t1 = 0;
        while (k == null) {
            //t0 = Clock.getInstance().now();
            k = new NativeBigInteger(ctx.keyGenerator().getElGamalExponentSize(), ctx.random());
            //t1 = Clock.getInstance().now();
            if (BigInteger.ZERO.compareTo(k) == 0) {
                k = null;
                continue;
            }
            BigInteger kPlus2 = k.add(_two);
            if (kPlus2.compareTo(CryptoConstants.elgp) > 0) k = null;
        }
        //long t2 = Clock.getInstance().now();
        y = CryptoConstants.elgg.modPow(k, CryptoConstants.elgp);

        BigInteger yk[] = new BigInteger[2];
        yk[0] = y;
        yk[1] = k;

        //long diff = t2 - t0;
        //if (diff > 1000) {
        //    if (_log.shouldLog(Log.WARN)) _log.warn("Took too long to generate YK value for ElGamal (" + diff + "ms)");
        //}

        return yk;
    }

/****
    private static final int RUNS = 500;

    public static void main(String args[]) {
        // warmup crypto
        ctx.random().nextInt();
        System.out.println("Begin YK generator speed test");
        long startNeg = Clock.getInstance().now();
        for (int i = 0; i < RUNS; i++) {
            getNextYK();
        }
        long endNeg = Clock.getInstance().now();
        long  negTime = endNeg - startNeg;
        // 14 ms each on a 2008 netbook (with jbigi)
        System.out.println("YK fetch time for " + RUNS + " runs: " + negTime + " @ " + (negTime / RUNS) + "ms each");
    }
****/

    final class YKPrecalcJob extends JobImpl {

        private YKPrecalcJob() {
            super(ctx);
        }

        int itemsAdded = 0;
        int tofill = 0;

        public void runJob() {
            if (tofill == 0)
                tofill = MAX_NUM_BUILDERS - _values.size();
            itemsAdded += tofill;
            do {
                _values.offer(generateYK());
            } while (--tofill > 0);
            tofill = MAX_NUM_BUILDERS - _values.size();
            if (tofill == 0) {
                if (itemsAdded > MIN_NUM_BUILDERS) {
                    int newmax = itemsAdded + ((itemsAdded + 1) >> 1);
                    if (newmax <= _values.getCapacity()) {
                        MIN_NUM_BUILDERS = itemsAdded;
                        MAX_NUM_BUILDERS = newmax;
                    }
                }
                itemsAdded = 0;
                _jobRunning.set(false);
            } else
                ctx.jobQueue().addJob(this);
        }

        public String getName() { return "YK Key Precalc"; }
    }
}
