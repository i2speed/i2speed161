package net.i2p.router;
/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain 
 * with no warranty of any kind, either expressed or implied.  
 * It probably won't make your computer catch on fire, or eat 
 * your children, but it might.  Use at your own risk.
 *
 */

import java.util.concurrent.atomic.AtomicLong;

/**
 * Base implementation of a Job
 *
 * For use by the router only. Not to be used by applications or plugins.
 */
public abstract class JobImpl implements Job {
    private final RouterContext _context;
    private static final AtomicLong _idSrc = new AtomicLong();
    private final long _id;
    private long _jobStart;
    private long _updatedStart;
    private long _actualStart;
    private long _actualEnd;
    
    public JobImpl(RouterContext context) {
        _context = context;
        _id = _idSrc.incrementAndGet();
    }
    
    public long getJobId() { return _id; }
    
    public final RouterContext getContext() { return _context; }
    
    /**
     * # of milliseconds after the epoch to start the job
     *
     */
    public long getStartAfter() { return _jobStart; }

    /**
     * WARNING - this does not force a resort of the job queue any more...
     * ALWAYS call JobImpl.requeue() instead if job is already queued.
     */
    public void setStartAfter(long startTime) {
        if (_jobStart == 0)
            _jobStart = startTime;
        else
            _updatedStart = startTime;
    }

    public void forceStartAfter(long startTime) {
        _jobStart = startTime;
        _updatedStart = 0;
        }

    public void updateStart() {
        _jobStart = _updatedStart;
        _updatedStart = 0;
    }

    public boolean isUpdated() {
        return _updatedStart > 0;
    }

    /**
     * # of milliseconds after the epoch the job actually started
     *
     */
    public long getActualStart() { return _actualStart; }
    public void setActualStart(long actualStartTime) { _actualStart = actualStartTime; }
    /**
     * Notify the timing that the job began
     *
     */
    public void start(long now) { _actualStart = now; }

    /**
     * # of milliseconds after the epoch the job actually ended
     *
     */
    public long getActualEnd() { return _actualEnd; }
    public void setActualEnd(long actualEndTime) { _actualEnd = actualEndTime; }

    /**
     * Notify the timing that the job finished
     *
     */
    public void end(long now) {
        _actualEnd = now;
    }

    public void offsetChanged(long delta) {
        if (_jobStart != 0)
            _jobStart += delta;
        if (_actualStart != 0)
            _actualStart += delta;
        if (_actualEnd != 0)
            _actualEnd += delta;
    }

    @Override
    public String toString() { 
        StringBuilder buf = new StringBuilder(128);
        buf.append(getClass().getSimpleName());
        buf.append(": Job ").append(_id).append(": ").append(getName());
        return buf.toString();
    }
    
    /**
     *  @deprecated
     *  @return null always
     */
    @Deprecated
    @SuppressWarnings("deprecation")
    public Exception getAddedBy() { return null; }

    public void dropped() {}
    
    /**
     *  Warning - only call this from runJob() or if Job is not already queued,
     *  or else it gets the job queue out of order.
     */
    protected void requeue(long delayMs) { 
        setStartAfter(_context.clock().now() + delayMs);
        _context.jobQueue().addJob(this);
    }
}
