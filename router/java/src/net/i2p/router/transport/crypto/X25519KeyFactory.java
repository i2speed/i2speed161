package net.i2p.router.transport.crypto;

import java.util.concurrent.atomic.*;

import net.i2p.crypto.EncType;
import net.i2p.crypto.KeyFactory;
import net.i2p.crypto.KeyPair;
import net.i2p.data.KeysAndCert;
import net.i2p.data.PrivateKey;
import net.i2p.data.PublicKey;
import net.i2p.router.RouterContext;
import net.i2p.util.I2PThread;
import net.i2p.util.Log;
import net.i2p.util.LockFreeQueue;
import net.i2p.util.SystemVersion;
import net.i2p.router.JobImpl;
import net.i2p.router.Job;

/**
 *  Try to keep DH pairs at the ready.
 *  It's important to do this in a separate thread, because if we run out,
 *  the pairs are generated in the NTCP Pumper thread,
 *  and it can fall behind.
 *
 *  @since 0.9.36 from DHSessionKeyFactory.PrecalcRunner
 */
public class X25519KeyFactory implements KeyFactory {

    private final RouterContext _context;
    private final Log _log;
    private int _minSize = 0;
    private int _maxSize = 1;
    private final LockFreeQueue<KeyPair> _keys;
    private boolean _isRunning;
    private AtomicBoolean _jobRunning = new AtomicBoolean();
    private Job _job = new XDHPrecalcJob();

    public X25519KeyFactory(RouterContext ctx) {
        _context = ctx;
        _log = ctx.logManager().getLog(X25519KeyFactory.class);
        ctx.statManager().createRateStat("crypto.XDHGenerateTime", "How long it takes to create x and X", "Encryption", new long[] { 60*60*1000 });
        ctx.statManager().createRateStat("crypto.XDHUsed", "Need a DH from the queue", "Encryption", new long[] { 60*60*1000 });
        ctx.statManager().createRateStat("crypto.XDHReused", "Unused DH requeued", "Encryption", new long[] { 60*60*1000 });
        ctx.statManager().createRateStat("crypto.XDHEmpty", "DH queue empty", "Encryption", new long[] { 60*60*1000 });

        _keys = new LockFreeQueue<KeyPair>(999);
    }
        
    /**
     *  Note that this stops the singleton precalc thread.
     *  You don't want to do this if there are multiple routers in the JVM.
     *  Fix this if you care. See Router.shutdown().
     */
    public synchronized void shutdown() {
        _isRunning = false;
        _keys.clear();
    }

    public synchronized void startup() {
        _isRunning = true;
        }
    final class XDHPrecalcJob extends JobImpl {

        private XDHPrecalcJob() {
            super(_context);
    }

        int itemsAdded = 0;
        int tofill = 0;

        public void runJob() {
            if (tofill <= 0)
                tofill = _maxSize - _keys.size();
            itemsAdded += tofill;
            do {
                _keys.offer(precalc());
            } while (--tofill > 0);
            tofill = _maxSize - _keys.size();
            if (tofill <= 0) {
                if (itemsAdded > _minSize) {
                    int newmax = itemsAdded + ((itemsAdded + 1) >> 1);
                    if (newmax <= _keys.getCapacity()) {
                        _minSize = itemsAdded;
                        _maxSize = newmax;
                    }
                }
                itemsAdded = 0;
                _jobRunning.set(false);
            } else
                _context.jobQueue().addJob(this);
        }

        public String getName() { return "XDH Session Key Precalc"; }
    }

    /**
     * Pulls a prebuilt keypair from the queue,
     * or if not available, construct a new one.
     */
    public KeyPair getKeys() {
        if (_keys.size() <= _minSize && _isRunning && _jobRunning.compareAndSet(false, true))
            _context.jobQueue().prioJob(_job);
        _context.statManager().addRateData("crypto.XDHUsed", 1);
        KeyPair rv = _keys.threaded_poll(false);
        if (rv == null) {
            _context.statManager().addRateData("crypto.XDHEmpty", 1);
            rv = precalc();
        }
        return rv;
    }

    private KeyPair precalc() {
        long start = _context.clock().now();
        KeyPair rv = _context.keyGenerator().generatePKIKeys(EncType.ECIES_X25519);
        long end = _context.clock().now();
        long diff = end - start;
        _context.statManager().addRateData("crypto.XDHGenerateTime", diff);
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("Took " + diff + "ms to generate local DH value");
        return rv;
    }

    /**
     * Return an unused DH key builder
     * to be put back onto the queue for reuse.
     */
    public void returnUnused(KeyPair kp) {
        if (_keys.offer(kp))
            _context.statManager().addRateData("crypto.XDHReused", 1);
    }

    /** @return true if successful, false if full */
    private final boolean addKeys(KeyPair kp) {
        return _keys.offer(kp);
    }

}
