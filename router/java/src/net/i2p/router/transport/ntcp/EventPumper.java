package net.i2p.router.transport.ntcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Inet6Address;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.Buffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.NoConnectionPendingException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.UnresolvedAddressException;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;

import net.i2p.I2PAppContext;
import net.i2p.data.ByteArray;
import net.i2p.data.router.RouterAddress;
import net.i2p.data.router.RouterIdentity;
import net.i2p.router.CommSystemFacade.Status;
import net.i2p.router.RouterContext;
import net.i2p.router.transport.FIFOBandwidthLimiter;
import net.i2p.util.LockFreeCache;
import net.i2p.util.Addresses;
import net.i2p.util.I2PThread;
import net.i2p.util.Log;
import net.i2p.util.ObjectCounter;
import net.i2p.util.SystemVersion;

/**
 *  The main NTCP NIO thread.
 */
class EventPumper implements Runnable {
    private final RouterContext _context;
    private final Log _log;
    private boolean _alive;
    private Selector _selector;
    private final NTCPTransport _transport;
    private final ObjectCounter<ByteArray> _blockedIPs;
    private long _expireIdleWriteTime;
    private static final boolean _useDirect = false;
    private final boolean _nodelay;
    // following to steer the pumpers use of sleeping through select()
    // and to handle any work due ASAP
    // _selector.wakeup() is expensive, only call if needed
    private ByteBuffer _readBuffer;

    /**
     *  This probably doesn't need to be bigger than the largest typical
     *  message, which is a 5-slot VTBM (~2700 bytes).
     *  The occasional larger message can use multiple buffers.
     */
    private static final int BUF_SIZE = 64*1024;

    private static class BufferFactory implements LockFreeCache.ObjectFactory<ByteBuffer> {
        public ByteBuffer newInstance() {
            if (_useDirect) 
                return ByteBuffer.allocateDirect(BUF_SIZE);
            else
                return ByteBuffer.allocate(BUF_SIZE);
        }
    }

    /** 
     * every few seconds, iterate across all ntcp connections just to make sure
     * we have their interestOps set properly (and to expire any looong idle cons).
     * as the number of connections grows, we should try to make this happen
     * less frequently (or not at all), but while the connection count is small,
     * the time to iterate across them to check a few flags shouldn't be a problem.
     */
    private static final int FAILSAFE_ITERATION_FREQ = 4*1000;
    private static final int SELECTOR_LOOP_DELAY = 20;
    private static final int MIN_FAILSAFE_WAITS = 100;
    private static final long BLOCKED_IP_FREQ = 60*1000;

    /** tunnel test now disabled, but this should be long enough to allow an active tunnel to get started */
    private static final int MIN_EXPIRE_IDLE_TIME = 120*1000;
    private static final int MAX_EXPIRE_IDLE_TIME = 11*60*1000;
    private static final int MAY_DISCON_TIMEOUT = 10*1000;

    /**
     *  Do we use direct buffers for reading? Default false.
     *  NOT recommended as we don't keep good track of them so they will leak.
     *
     *  Unsupported, set _useDirect above.
     *
     *  @see java.nio.ByteBuffer
     */
    //private static final String PROP_DIRECT = "i2np.ntcp.useDirectBuffers";
    private static final String PROP_NODELAY = "i2np.ntcp.nodelay";

    private static final int MIN_MINB = 4;
    private static final int MAX_MINB = 32;
    private static final int MIN_BUFS;
    static {
        long maxMemory = SystemVersion.getMaxMemory();
        MIN_BUFS = (int) Math.max(MIN_MINB, Math.min(MAX_MINB, 1 + (maxMemory / (8*1024*1024))));
    }
    
    private static final LockFreeCache<ByteBuffer> _bufferCache = new LockFreeCache<ByteBuffer>(new BufferFactory(), MIN_BUFS);

    private static final Set<Status> STATUS_OK =
        EnumSet.of(Status.OK, Status.IPV4_OK_IPV6_UNKNOWN, Status.IPV4_OK_IPV6_FIREWALLED);

    public EventPumper(RouterContext ctx, NTCPTransport transport) {
        _context = ctx;
        _log = ctx.logManager().getLog(getClass());
        _transport = transport;
        _expireIdleWriteTime = MAX_EXPIRE_IDLE_TIME;
        _blockedIPs = new ObjectCounter<ByteArray>();
        _context.statManager().createRateStat("ntcp.pumperKeySetSize", "", "ntcp", new long[] {10*60*1000} );
        //_context.statManager().createRateStat("ntcp.pumperKeysPerLoop", "", "ntcp", new long[] {10*60*1000} );
        _context.statManager().createRateStat("ntcp.pumperLoopsPerSecond", "", "ntcp", new long[] {10*60*1000} );
        _context.statManager().createRateStat("ntcp.zeroRead", "", "ntcp", new long[] {10*60*1000} );
        _context.statManager().createRateStat("ntcp.zeroReadDrop", "", "ntcp", new long[] {10*60*1000} );
        _context.statManager().createRateStat("ntcp.dropInboundNoMessage", "", "ntcp", new long[] {10*60*1000} );
        _nodelay = ctx.getBooleanPropertyDefaultTrue(PROP_NODELAY);
        _readBuffer = acquireBuf();
    }
    
    public synchronized void startPumping() {
        if (_log.shouldLog(Log.INFO))
            _log.info("Starting pumper");
        try {
            _selector = Selector.open();
            _alive = true;
            new I2PThread(this, "NTCP Pumper", true).start();
        } catch (IOException ioe) {
            _log.log(Log.CRIT, "Error opening the NTCP selector", ioe);
        } catch (java.lang.InternalError jlie) {
            // "unable to get address of epoll functions, pre-2.6 kernel?"
            _log.log(Log.CRIT, "Error opening the NTCP selector", jlie);
        }
    }
    
    public synchronized void stopPumping() {
        _alive = false;
        if (_selector != null && _selector.isOpen())
            _selector.wakeup();
    }
    
    /**
     *  Selector can take quite a while to close after calling stopPumping()
     */
    public boolean isAlive() {
        return _alive || (_selector != null && _selector.isOpen());
    }

    /**
     *  Wake up the pumper if sleeping. 
     */
    public void wakeUp() {
        _selector.wakeup();
    }

    /**
     *  Register the acceptor.
     *  This is only called from NTCPTransport.bindAddress(), so it isn't clear
     *  why this needs a queue. 
     */
    public void register(ServerSocketChannel chan) {
        if (_log.shouldLog(Log.DEBUG)) _log.debug("Registering server socket channel");
        // only when address changes
        try {
            chan.register(_selector, SelectionKey.OP_ACCEPT, chan);
        } catch (ClosedChannelException cce) {
            if (_log.shouldLog(Log.WARN)) _log.warn("Error registering", cce);
        }
    }

    /**
     *  Outbound
     */
    public void registerConnect(NTCPConnection con, SocketChannel schan) {
        try {
            con.setChannel(schan);
            RouterAddress naddr = con.getRemoteAddress();
            try {
                // no DNS lookups, do not use host names
                int port = naddr.getPort();
                byte[] ip = naddr.getIP();
                if (port <= 0 || ip == null)
                    throw new IOException("Invalid NTCP address: " + naddr);
                InetSocketAddress saddr = new InetSocketAddress(InetAddress.getByAddress(ip), port);
                schan.connect(saddr);
            } catch (IOException ioe) {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("error connecting to " + Addresses.toString(naddr.getIP(), naddr.getPort()), ioe);
                _context.statManager().addRateData("ntcp.connectFailedIOE", 1);
                _transport.markUnreachable(con.getRemotePeer().calculateHash());
                con.close(true);
            } catch (UnresolvedAddressException uae) {                    
                if (_log.shouldLog(Log.WARN)) _log.warn("unresolved address connecting", uae);
                _context.statManager().addRateData("ntcp.connectFailedUnresolved", 1);
                _transport.markUnreachable(con.getRemotePeer().calculateHash());
                con.close(true);
            }
            SelectionKey key = schan.register(_selector, SelectionKey.OP_CONNECT, con);
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("Registering " + con);
            _context.statManager().addRateData("ntcp.registerConnect", 1);
            con.setKey(key);
        } catch (ClosedChannelException cce) {
            if (_log.shouldLog(Log.WARN)) _log.warn("Error registering", cce);
        }
    }
    
    /**
     *  The selector loop.
     *  On high-bandwidth routers, this is the thread with the highest CPU usage, so
     *  take care to minimize overhead and unnecessary debugging stuff.
     */
    public void run() {
        int loopCount = 0;
        long lastFailsafeIteration = _context.clock().now();
        long lastBlockedIPClear = lastFailsafeIteration;
        long now = lastFailsafeIteration;
        while (_alive && _selector.isOpen()) {
            try {
                loopCount++;

                try {
                    int count = _selector.selectNow();
                    if (count == 0)
                        // we only get here if no wakeup pending, so select() will not fall through
                        count = _selector.select(SELECTOR_LOOP_DELAY);
                    if (count > 0) {
                        Set<SelectionKey> selected = _selector.selectedKeys();
                        //_context.statManager().addRateData("ntcp.pumperKeysPerLoop", selected.size());
                        processKeys(selected);
                        selected.clear();
                    }
                } catch (ClosedSelectorException cse) {
                    continue;
                } catch (IOException ioe) {
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Error selecting", ioe);
                } catch (CancelledKeyException cke) {
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Error selecting", cke);
                    continue;
                }
                
                now = _context.clock().now();
                if (lastFailsafeIteration + FAILSAFE_ITERATION_FREQ < now) {
                    // in the *cough* unthinkable possibility that there are bugs in
                    // the code, lets periodically pass over all NTCP connections and
                    // make sure that anything which should be able to write has been
                    // properly marked as such, etc
                    lastFailsafeIteration = now;
                    try {
                        Set<SelectionKey> all = _selector.keys();
                        int lastKeySetSize = all.size();
                        _context.statManager().addRateData("ntcp.pumperKeySetSize", lastKeySetSize);
                        _context.statManager().addRateData("ntcp.pumperLoopsPerSecond", loopCount / (FAILSAFE_ITERATION_FREQ / 1000));
                        // reset the failsafe loop counter,
                        // and recalculate the max loops before failsafe sleep, based on number of keys
                        loopCount = 0;
                        
                        int failsafeWrites = 0;
                        int failsafeCloses = 0;
                        int failsafeInvalid = 0;

                        // Increase allowed idle time if we are well under allowed connections, otherwise decrease
                        boolean haveCap = _transport.haveCapacity(95);
                        if (haveCap)
                            _expireIdleWriteTime = Math.min(_expireIdleWriteTime + 1000, MAX_EXPIRE_IDLE_TIME);
                        else
                            _expireIdleWriteTime = Math.max(_expireIdleWriteTime - 3000, MIN_EXPIRE_IDLE_TIME);
                        for (SelectionKey key : all) {
                            try {
                                Object att = key.attachment();
                                if (!(att instanceof NTCPConnection))
                                    continue; // to the next con
                                NTCPConnection con = (NTCPConnection)att;
                                
                                /**
                                 * 100% CPU bug
                                 * http://forums.java.net/jive/thread.jspa?messageID=255525
                                 * http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6595055
                                 * 
                                 * The problem is around a channel that was originally registered with Selector for i/o gets
                                 * closed on the server side (due to early client side exit).  But the server side can know
                                 * about such channel only when it does i/o (read/write) and thereby getting into an IO exception.
                                 * In this case, (bug 6595055)there are times (erroneous) when server side (selector) did not
                                 * know the channel is already closed (peer-reset), but continue to do the selection cycle on
                                 * a key set whose associated channel is alreay closed or invalid. Hence, selector's slect(..)
                                 * keep spinging with zero return without blocking for the timeout period.
                                 * 
                                 * One fix is to have a provision in the application, to check if any of the Selector's keyset
                                 * is having a closed channel/or invalid registration due to channel closure.
                                 */
                                if ((!key.isValid()) &&
                                    (!((SocketChannel)key.channel()).isConnectionPending()) &&
                                    con.getTimeSinceCreated(now) > 2 * NTCPTransport.ESTABLISH_TIMEOUT) {
                                    if (_log.shouldLog(Log.INFO))
                                        _log.info("Removing invalid key for " + con);
                                    // this will cancel the key, and it will then be removed from the keyset
                                    con.close();
                                    key.cancel();
                                    failsafeInvalid++;
                                    continue;
                                }

                                boolean connected = ((SocketChannel)key.channel()).isConnected();
                                if (con.getMessagesReceived() == 0) {
                                    boolean inbound = con.isInbound();
                                    int lastReceive = con.getTimeSinceReceive(now);
                                    if (connected && inbound && lastReceive > 500) {
                                        processRead(key);
                                        if (_log.shouldWarn())
                                            _log.warn("Failsafe read for " + con);
                                        continue;
                                    }
                                    if (!connected && !inbound && lastReceive > 3000) {
                                        processConnect(key);
                                        if (_log.shouldInfo())
                                            _log.info("Failsafe connect for " + con);
                                        con.delayedSend();
                                        continue;
                                    }
                                };

                                if (!con.isEstablished()) continue; // avoid races during establishment

                                boolean needsWrite = !con.isWriteBufEmpty();
                                int writeDelay = con.getTimeSinceSend(now);

                                if (needsWrite && writeDelay > 9000 ||
                                     writeDelay > _expireIdleWriteTime &&
                                     con.getTimeSinceReceive(now) > _expireIdleWriteTime) {
                                    // we haven't sent or received anything in a really long time, so lets just close 'er up
                                    // con will cancel the key
                                    con.sendTerminationAndClose();
                                    if (_log.shouldInfo())
                                        _log.info("Failsafe or expire close for " + con);
                                    failsafeCloses++;
                                    continue;
                                }

                                if ((needsWrite || con.getOutboundQueueSize() > 0) && 
                                    connected && writeDelay > 1000) {
                                   // the data queued to be sent has already passed through
                                   // the bw limiter and really just wants to get shoved
                                   // out the door asap.
                                   if (_log.shouldLog(Log.INFO))
                                       _log.info("Failsafe write for " + con);
                                   con.delayedSend();
                                   failsafeWrites++;
                               }
                            } catch (CancelledKeyException cke) {
                                // cancelled while updating the interest ops.  ah well
                            }
                        }
                        if (failsafeWrites > 0)
                            _context.statManager().addRateData("ntcp.failsafeWrites", failsafeWrites);
                        if (failsafeCloses > 0)
                            _context.statManager().addRateData("ntcp.failsafeCloses", failsafeCloses);
                        if (failsafeInvalid > 0)
                            _context.statManager().addRateData("ntcp.failsafeInvalid", failsafeInvalid);
                    } catch (ClosedSelectorException cse) {
                        continue;
                    }
                    if (lastBlockedIPClear + BLOCKED_IP_FREQ < now) {
                        _blockedIPs.clear();
                        lastBlockedIPClear = now;
                    }

                    expireTimedOut();

                    // sleep a bit if we did not wait enough above
/*                     if ((failSafeWaits -= MIN_FAILSAFE_WAITS) < 0) {
                        if (_log.shouldLog(Log.INFO))
                            _log.info("EventPumper throttle " + loopCount + " loops in " +
                                      (now - lastFailsafeIteration) + " ms");
                        _context.statManager().addRateData("ntcp.failsafeThrottle", 1);
                        try {
                            Thread.sleep(-failSafeWaits);
                        } catch (InterruptedException ie) {}
                    }
                    failSafeWaits = 0;
 */                }
            } catch (RuntimeException re) {
                _log.error("Error in the event pumper", re);
            }
        }
        try {
            if (_selector.isOpen()) {
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("Closing down the event pumper with selection keys remaining");
                Set<SelectionKey> keys = _selector.keys();
                for (SelectionKey key : keys) {
                    try {
                        Object att = key.attachment();
                        if (att instanceof ServerSocketChannel) {
                            ServerSocketChannel chan = (ServerSocketChannel)att;
                            chan.close();
                            key.cancel();
                        } else if (att instanceof NTCPConnection) {
                            NTCPConnection con = (NTCPConnection)att;
                            con.close();
                            key.cancel();
                        }
                    } catch (IOException ke) {
                        _log.error("Error closing key " + key + " on pumper shutdown", ke);
                    }
                }
                _selector.close();
            } else {
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("Closing down the event pumper with no selection keys remaining");
            }
        } catch (IOException e) {
            _log.error("Error closing keys on pumper shutdown", e);
        }
    }
    
    /**
     *  Process all keys from the last select.
     *  High-frequency path in thread.
     */
    private void processKeys(Set<SelectionKey> selected) {
        Iterator<SelectionKey> iter = selected.iterator();
        do {  
            SelectionKey key = iter.next();  
            try {
                int ops = key.readyOps();
                boolean accept = (ops & SelectionKey.OP_ACCEPT) != 0;
                boolean connect = (ops & SelectionKey.OP_CONNECT) != 0;
                boolean read = (ops & SelectionKey.OP_READ) != 0;
                boolean write = (ops & SelectionKey.OP_WRITE) != 0;
                //if (_log.shouldLog(Log.DEBUG))
                //    _log.debug("ready ops for : " + key
                //               + " accept? " + accept + " connect? " + connect
                //               + " read? " + read 
                //               + "/" + ((key.interestOps()&SelectionKey.OP_READ)!= 0)
                //               + " write? " + write 
                //               + "/" + ((key.interestOps()&SelectionKey.OP_WRITE)!= 0)
                //               + " on " + key.attachment()
                //               );
                if (accept) {
                    _context.statManager().addRateData("ntcp.accept", 1);
                    processAccept(key);
                }
                if (connect) {
                    key.interestOpsAnd(~SelectionKey.OP_CONNECT);
                    processConnect(key);
                }
                if (read) {
                    processRead(key);
                }
                if (write) processWrite(key);
                //if (!(accept || connect || read || write)) {
                //    if (_log.shouldLog(Log.INFO))
                //        _log.info("key wanted nothing? con: " + key.attachment());
                //}
            } catch (CancelledKeyException cke) {
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("key cancelled");
            }
        } while (iter.hasNext());
    }

    /**
     *  Called by the connection when it has data ready to write.
     *  If we have bandwidth, calls con.Write() which calls wantsWrite(con).
     *  If no bandwidth, calls con.queuedWrite().
     */
    // public void wantsWrite(NTCPConnection con, byte data[], boolean direct) {
    //     wantsWrite(con, data, 0, data.length, direct);
    // }

    /**
     *  Called by the connection when it has data ready to write.
     *  If we have bandwidth, calls con.Write() which calls wantsWrite(con).
     *  If no bandwidth, calls con.queuedWrite().
     *
     *  @since 0.9.35 off/len version
     */
    // public void wantsWrite(NTCPConnection con, byte data[], int off, int len, boolean direct) {
    //     ByteBuffer buf = ByteBuffer.wrap(data, off, len);
    //     FIFOBandwidthLimiter.Request req = _context.bandwidthLimiter().requestOutbound(len, 0, "NTCP write");//con, buf);
    //     if (req != null && req.getPendingRequested() > 0) {
    //         if (_log.shouldLog(Log.INFO))
    //             _log.info("queued write on " + con + " for " + len);
    //         _context.statManager().addRateData("ntcp.wantsQueuedWrite", 1);
    //         con.queuedWrite(buf, req);
    //     } else {
    //         con.write(buf, direct);
    //     }
    // }

    /**
     *  Called by the connection when it has data ready to write (after bw allocation).
     *  Only wakeup if new.
     */
    // public void wantsWrite(NTCPConnection con) {
    //     con.setWriteInterest();
    //     wakeUp();
    // }

    /**
     *  This is only called from NTCPConnection.complete()
     *  if there is more data, which is rare (never?)
     *  so we don't need to check for dups or make _wantsRead a Set.
     */
    public void wantsRead(NTCPConnection con) {
        try {
            SelectionKey conKey = con.getKey();
            if (conKey != null && conKey.isValid()) {
                conKey.interestOpsOr(SelectionKey.OP_READ);
            }
        } catch (CancelledKeyException cke) {
            return;
        }
        wakeUp();
    }

    /**
     *  High-frequency path in thread.
     */
    private ByteBuffer acquireBuf() {
        return _bufferCache.poll();
    }
    
    /**
     *  Return a read buffer to the pool.
     *  These buffers must be from acquireBuf(), i.e. capacity() == BUF_SIZE.
     *  High-frequency path in thread.
     */
    public void releaseBuf(ByteBuffer buf) {
        // double check
        if (buf.capacity() < BUF_SIZE) {
            I2PAppContext.getGlobalContext().logManager().getLog(EventPumper.class).error("Bad size " + buf.capacity(), new Exception());
            return;
        }
        buf.clear();
        _bufferCache.offer(buf);
    }
    
    private void processAccept(SelectionKey key) {
        ServerSocketChannel servChan = (ServerSocketChannel)key.attachment();
        try {
            SocketChannel chan = servChan.accept();
            // don't throw an NPE if the connect is gone again
            if(chan == null)
                return;
            chan.configureBlocking(false);
            SelectionKey ckey = chan.register(_selector, SelectionKey.OP_READ);

            if (!_transport.allowConnection()) {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Receive session request but at connection limit: " + chan.socket().getInetAddress());
                try { chan.close(); } catch (IOException ioe) { }
                return;
            }

            byte[] ip = chan.socket().getInetAddress().getAddress();
            if (_context.blocklist().isBlocklisted(ip)) {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Receive session request from blocklisted IP: " + chan.socket().getInetAddress());
                try { chan.close(); } catch (IOException ioe) { }
                return;
            }

            ByteArray ba = new ByteArray(ip);
            int count = _blockedIPs.count(ba);
            if (count > 0) {
                count = _blockedIPs.increment(ba);
                if (_log.shouldLog(Log.WARN))
                   _log.warn("Blocking accept of IP with count " + count + ": " + Addresses.toString(ip));
                _context.statManager().addRateData("ntcp.dropInboundNoMessage", count);
                try { chan.close(); } catch (IOException ioe) { }
                return;
            }

            if (shouldSetKeepAlive(chan))
                chan.socket().setKeepAlive(true);
            if (_nodelay)
                chan.socket().setTcpNoDelay(true);

            NTCPConnection con = new NTCPConnection(_context, _transport, chan, ckey);
            ckey.attach(con);
            _transport.establishing(con);
        } catch (IOException ioe) {
            _log.error("Error accepting", ioe);
        }
    }
    
    private void processConnect(SelectionKey key) {
        final NTCPConnection con = (NTCPConnection)key.attachment();
        final SocketChannel chan = con.getChannel();
        try {
            boolean connected = chan.finishConnect();
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("processing connect for " + con + ": connected? " + connected);
            if (connected) {
                if (shouldSetKeepAlive(chan))
                    chan.socket().setKeepAlive(true);
                if (_nodelay)
                    chan.socket().setTcpNoDelay(true);
                // key was already set when the channel was created, why do it again here?
                con.setKey(key);
                con.outboundConnected();
                _context.statManager().addRateData("ntcp.connectSuccessful", 1);
            } else {
                con.closeOnTimeout("connect failed", null);
                _transport.markUnreachable(con.getRemotePeer().calculateHash());
                _context.statManager().addRateData("ntcp.connectFailedTimeout", 1);
            }
        } catch (IOException ioe) {   // this is the usual failure path for a timeout or connect refused
            if (_log.shouldLog(Log.INFO))
                _log.info("Failed outbound " + con, ioe);
            con.closeOnTimeout("connect failed", ioe);
            _transport.markUnreachable(con.getRemotePeer().calculateHash());
            _context.statManager().addRateData("ntcp.connectFailedTimeoutIOE", 1);
        } catch (NoConnectionPendingException ncpe) {
            // ignore
            if (_log.shouldLog(Log.WARN))
                _log.warn("error connecting on " + con, ncpe);
        }
    }

    /**
     *  @since 0.9.20
     */
    private boolean shouldSetKeepAlive(SocketChannel chan) {
        if (chan.socket().getInetAddress() instanceof Inet6Address)
            return false;
        Status status = _context.commSystem().getStatus();
        return !STATUS_OK.contains(status);
    }

    /**
     *  OP_READ will always be set before this is called.
     *  This method will disable the interest if no more reads remain because of inbound bandwidth throttling.
     *  High-frequency path in thread.
     */
    private void processRead(SelectionKey key) {
        final NTCPConnection con = (NTCPConnection)key.attachment();
        final SocketChannel chan = con.getChannel();
        try {
            _readBuffer.clear();
            int read = chan.read(_readBuffer);
            if (_log.shouldDebug())
                _log.debug("Read " + read + " bytes total from " + con);
            if (read > 0) {
                // Process the data received
                // clear counter for workaround above
                con.clearZeroRead();
                // ZERO COPY. The buffer will be returned in Reader.processRead()
                // not ByteBuffer to avoid Java 8/9 issues with flip()
                ((Buffer)_readBuffer).flip();
                FIFOBandwidthLimiter.Request req = _context.bandwidthLimiter().requestInbound(read, "NTCP read"); //con, buf);
                if (req != null) {
                    // rare since we generally don't throttle inbound
                    key.interestOpsAnd(~SelectionKey.OP_READ);
                    _context.statManager().addRateData("ntcp.queuedRecv", read);
                    con.queuedRecv(_readBuffer, req);
                    _readBuffer = acquireBuf();
                } else {
                    // stay interested
                    //key.interestOps(key.interestOps() | SelectionKey.OP_READ);
                    con.recv(_readBuffer);
                    _context.statManager().addRateData("ntcp.read", read);
                }
            } else if (read < 0) {
                if (con.isInbound() && con.getMessagesReceived() <= 0) {
                    InetAddress addr = chan.socket().getInetAddress();
                    int count;
                    if (addr != null && con.getUptime() < 7000) { // do not block on timeouts
                        byte[] ip = addr.getAddress();
                        ByteArray ba = new ByteArray(ip);
                        count = _blockedIPs.increment(ba);
                        if (_log.shouldLog(Log.WARN))
                            _log.warn("EOF on inbound before receiving any, blocking IP " + Addresses.toString(ip) + " with count " + count + ": " + con);
                    } else {
                        count = 1;
                        if (_log.shouldLog(Log.WARN))
                            _log.warn("EOF on inbound before receiving any: " + con);
                    }
                    _context.statManager().addRateData("ntcp.dropInboundNoMessage", count);
                } else {
                    if (_log.shouldLog(Log.DEBUG))
                        _log.debug("EOF on " + con);
                }
                con.close();
            } else {
                    // stay interested
                    //key.interestOps(key.interestOps() | SelectionKey.OP_READ);
                    // workaround for channel stuck returning 0 all the time, causing 100% CPU
                    int consec = con.gotZeroRead();
                    if (consec >= 5) {
                        _context.statManager().addRateData("ntcp.zeroReadDrop", 1);
                        if (_log.shouldLog(Log.WARN))
                            _log.warn("Fail safe zero read close " + con);
                        con.close();
                    } else {
                        _context.statManager().addRateData("ntcp.zeroRead", consec);
                        if (_log.shouldLog(Log.INFO))
                            _log.info("nothing to read for " + con + ", but stay interested");
                    }
                }
        } catch (CancelledKeyException cke) {
            if (_log.shouldLog(Log.WARN)) _log.warn("error reading on " + con, cke);
            con.close();
            _context.statManager().addRateData("ntcp.readError", 1);
        } catch (IOException ioe) {
            // common, esp. at outbound connect time
            if (con.isInbound() && con.getMessagesReceived() <= 0) {
                InetAddress addr = chan.socket().getInetAddress();
                int count;
                if (addr != null) {
                    byte[] ip = addr.getAddress();
                    ByteArray ba = new ByteArray(ip);
                    count = _blockedIPs.increment(ba);
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Blocking IP " + Addresses.toString(ip) + " with count " + count + ": " + con, ioe);
                } else {
                    count = 1;
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("IOE on inbound before receiving any: " + con, ioe);
                }
                _context.statManager().addRateData("ntcp.dropInboundNoMessage", count);
            } else {
                if (_log.shouldLog(Log.INFO))
                    _log.info("error reading on " + con, ioe);
            }
            if (con.isEstablished()) {
                _context.statManager().addRateData("ntcp.readError", 1);
            } else {
                // Usually "connection reset by peer", probably a conn limit rejection?
                // although it could be a read failure during the DH handshake
                // Same stat as in processConnect()
                _context.statManager().addRateData("ntcp.connectFailedTimeoutIOE", 1);
                RouterIdentity rem = con.getRemotePeer();
                if (rem != null && !con.isInbound())
                    _transport.markUnreachable(rem.calculateHash());
            }
            con.close();
        } catch (NotYetConnectedException nyce) {
            // ???
            key.interestOps(key.interestOps() & ~SelectionKey.OP_READ);
            if (_log.shouldLog(Log.WARN))
                _log.warn("error reading on " + con, nyce);
        }
    }
    
    /**
     *  OP_WRITE will always be set before this is called.
     *  This method will disable the interest if no more writes remain.
     *  High-frequency path in thread.
     */
    private void processWrite(SelectionKey key) {
        final NTCPConnection con = (NTCPConnection)key.attachment();
        con.delayedSend();
    }
    
    private void expireTimedOut() {
        _transport.expireTimedOut();
    }

    public long getIdleTimeout() { return _expireIdleWriteTime; }
}
