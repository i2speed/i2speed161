package net.i2p.router.transport.udp;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.*;

import net.i2p.data.Hash;
import net.i2p.data.SessionKey;
import net.i2p.router.RouterContext;
import net.i2p.util.Log;
import net.i2p.util.LockFreeQueue;
import net.i2p.util.LockFreeRingBuffer;

/**
 * Contain all of the state about a UDP connection to a peer.
 * This is instantiated only after a connection is fully established.
 *
 * Public only for UI peers page. Not a public API, not for external use.
 *
 */
public class PeerState {
    private final RouterContext _context;
    private final Log _log;
    /**
     * The peer are we talking to.  This should be set as soon as this
     * state is created if we are initiating a connection, but if we are
     * receiving the connection this will be set only after the connection
     * is established.
     */
    private final Hash _remotePeer;
    /** 
     * The AES key used to verify packets, set only after the connection is
     * established.  
     */
    private SessionKey _currentMACKey;
    /**
     * The AES key used to encrypt/decrypt packets, set only after the 
     * connection is established.
     */
    private SessionKey _currentCipherKey;
    /** 
     * The pending AES key for verifying packets if we are rekeying the 
     * connection, or null if we are not in the process of rekeying.
     */
    private SessionKey _nextMACKey;
    /** 
     * The pending AES key for encrypting/decrypting packets if we are 
     * rekeying the connection, or null if we are not in the process 
     * of rekeying.
     */
    private SessionKey _nextCipherKey;

    /** when were the current cipher and MAC keys established/rekeyed? */
    private long _keyEstablishedTime;

    /**
     *  How far off is the remote peer from our clock, in milliseconds?
     *  A positive number means our clock is ahead of theirs.
     */
    private long _clockSkew;
    private final Object _clockSkewLock = new Object();

    /** when did we last send them a packet? */
    private long _lastSendTime;
    /** when did we last send them a message that was ACKed */
    private long _lastSendFullyTime;
    /** when did we last send them a ping? */
    private long _lastPingTime;
    /** when did we last receive a packet from them? */
    private long _lastReceiveTime;

    /** when did we last have a failed send (beginning of period) */
    // private long _lastFailedSendPeriod;

    /**
     *  List of messageIds (Long) that we have received
     * and will ACK twice along with the points in time.
     */
    private final LockFreeRingBuffer<Ack> _currentACKs;
    private final LockFreeRingBuffer<Ack> _resendACKs;
    /** when did we last send ACKs to the peer? */
    private long _lastACKSend;
    private final AtomicBoolean _lock = new AtomicBoolean(false);
    private long _nextInfoTime;
    /** 
     * have all of the packets received in the current second requested that
     * the previous second's ACKs be sent?
     */
    //private boolean _remoteWantsPreviousACKs;
    /** how many bytes should we send to the peer in a second */
    private int _sendWindowMessages;
    /** how many bytes can we send to the peer in the current second */
    private int _sendWindowMessagesRemaining;
    /** when did we last refill the send window */
    private long _lastSendRefill;
    /** when will we check for a resend */
    private long _resendCheck;
    /** how many messages have been ACKed */
    private int _messagesAcked;
    private int _oldMessagesAcked;
    // smoothed value, for display only
    private int _sendBps;
    private int _sendBytes;
    // smoothed value, for display only
    private int _receiveBps;
    private int _receiveBytes;
    /** speed: caches minimum (re-)send time of our messages and ACKs*/
    private long _nextSendTime;
    //private int _sendACKBps;
    //private int _sendZACKBytes;
    //private int _receiveACKBps;
    //private int _receiveACKBytes;
    //private long _receivePeriodBegin;
    /** what IP is the peer sending and receiving packets on? */
    private final byte[] _remoteIP;
    /** cached IP address */
    private InetAddress _remoteIPAddress;
    /** what port is the peer sending and receiving packets on? */
    private int _remotePort;
    /** cached RemoteHostId, used to find the peerState by remote info */
    private RemoteHostId _remoteHostId;

    /** if we need to contact them, do we need to talk to an introducer? */
    //private boolean _remoteRequiresIntroduction;

    /** 
     * if we are serving as an introducer to them, this is the the tag that
     * they can publish that, when presented to us, will cause us to send
     * a relay introduction to the current peer 
     */
    private long _weRelayToThemAs;
    /**
     * If they have offered to serve as an introducer to us, this is the tag
     * we can use to publish that fact.
     */
    private long _theyRelayToUsAs;
    /** what is the largest packet we can currently send to the peer? */
    private int _mtu;
    private int _mtuReceive;
    private int _fragmentsize;
    /** what is the largest packet we will ever send to the peer? */
    private int _largeMTU;
    /* how many consecutive packets at or under the min MTU have been received */
    private long _consecutiveSmall;
    /** when did we last check the MTU? */
    //private long _mtuLastChecked;
    /** current round trip time estimate */
    private int _rtt;
    /** smoothed mean deviation in the rtt */
    private int _rttDeviation;
    /** current retransmission timeout */
    private int _rto;
    
    
    private int _messagesReceived;
    private int _messagesSent;
    private int _packetsTransmitted;
    /** how many packets were retransmitted within the last RETRANSMISSION_PERIOD_WIDTH packets */
    private int _packetsRetransmitted;

    /** how many packets were transmitted within the last RETRANSMISSION_PERIOD_WIDTH packets */
    //private long _packetsPeriodTransmitted;
    //private int _packetsPeriodRetransmitted;
    //private int _packetRetransmissionRate;

    /** how many dup packets were received within the last RETRANSMISSION_PERIOD_WIDTH packets */
    private int _packetsReceivedDuplicate;
    private int _packetsReceived;
    private boolean _mayDisconnect;
    
    /** list of InboundMessageState for active message */
    private final ConcurrentLinkedQueue<InboundMessageState> _inboundMessages;

    // our sent messages that have not been ACKed and not stripped out yet
    // solves the sync between sending messages and receiving ACKs
    private int _retransmissions;
    private boolean _throttled;
    private long _lossEvent;
    private OutboundMessageState _oldestInPlay;

    /**
     *  Mostly messages that have been transmitted and are awaiting acknowledgement,
     *  although there could be some that have not been sent yet.
     */
    public final LinkedHashMap<Integer, OutboundMessageState> _outboundMessages;
    private final ConcurrentLinkedQueue<OutboundMessageState> _multiFrag;

    /**
     *  Priority queue of messages that have not yet been sent.
     *  They are taken from here and put in _outboundMessages.
     */
    //private final CoDelPriorityBlockingQueue<OutboundMessageState> _outboundQueue;
    private final LockFreeQueue<OutboundMessageState> _outboundQueue;

    /** when the retransmit timer is about to trigger */
    private long _retransmitTimer;

    private final UDPTransport _transport;
    
    /** have we migrated away from this peer to another newer one? */
    // private volatile boolean _dead;

    /** is it inbound? **/
    private final boolean _isInbound;
    /** Last time it was made an introducer **/
    private long _lastIntroducerTime;

    private static final int DEFAULT_SEND_WINDOW_MESSAGES = 8;
    private static final int MINIMUM_WINDOW_MESSAGES = DEFAULT_SEND_WINDOW_MESSAGES;
    private static final int MAX_SEND_WINDOW_BYTES = 1024*1024;
    /**
     * how frequently do we want to send ACKs to a peer? 
    */
    static final int ACK_FREQUENCY = 150;

    /** max number of msgs returned from allocateSend() */
    private static final int MAX_ALLOCATE_SEND = 20;

    /**
     *  Was 32 before 0.9.2, but since the streaming lib goes up to 128,
     *  we would just drop our own msgs right away during slow start.
     *  May need to adjust based on memory.
     */
    public static final int MAX_SEND_MSGS_PENDING = 128;

    /**
     * IPv4 Min MTU
     *
     * 596 gives us 588 IP byes, 568 UDP bytes, and with an SSU data message, 
     * 522 fragment bytes, which is enough to send a tunnel data message in 2 
     * packets. A tunnel data message sent over the wire is 1044 bytes, meaning 
     * we need 522 fragment bytes to fit it in 2 packets - add 46 for SSU, 20 
     * for UDP, and 8 for IP, giving us 596.  round up to mod 16, giving a total
     * of 608
     *
     * Well, we really need to count the acks as well, especially
     * 1 + (4 * MAX_RESEND_ACKS_SMALL) which can take up a significant amount of space.
     * We reduce the max acks when using the small MTU but it may not be enough...
     *
     * Goal: VTBM msg fragments 2646 / (620 - 87) fits nicely.
     *
     * Assuming that we can enforce an MTU correctly, this % 16 should be 12,
     * as the IP/UDP header is 28 bytes and data max should be mulitple of 16 for padding efficiency,
     * and so PacketBuilder.buildPacket() works correctly.
     */
    public static final int MIN_MTU = 620;

    /**
     * IPv6/UDP header is 48 bytes, so we want MTU % 16 == 0.
     */
    public static final int MIN_IPV6_MTU = 1280;
    public static final int MAX_IPV6_MTU = 1280;
    private static final int DEFAULT_MTU = MIN_MTU;

    /**
     * IPv4 Max MTU
     *
     * based on measurements, 1350 fits nearly all reasonably small I2NP messages
     * (larger I2NP messages may be up to 1900B-4500B, which isn't going to fit
     * into a live network MTU anyway)
     *
     * TODO
     * VTBM is 2646, it would be nice to fit in two large
     * 2646 / 2 = 1323
     * 1323 + 74 + 46 + 1 + (4 * 9) = 1480
     * So why not make it 1492 (old ethernet is 1492, new is 1500)
     * Changed to 1492 in 0.8.9
     *
     * BUT through 0.8.11,
     * size estimate was bad, actual packet was up to 48 bytes bigger
     * To be figured out. Curse the ACKs.
     * Assuming that we can enforce an MTU correctly, this % 16 should be 12,
     * as the IP/UDP header is 28 bytes and data max should be mulitple of 16 for padding efficiency,
     * and so PacketBuilder.buildPacket() works correctly.
     */
    public static final int LARGE_MTU = 1276;
    
    /**
     *  Max of IPv4 and IPv6 max MTUs
     *  @since 0.9.28
     */
    public static final int MAX_MTU = 1488;
    
    private static final int MIN_RTO = 1000;
    private static final int INIT_RTO = 1000;
    private static final int SEND_WINDOW = 1000;
    private static final int INIT_RTT = MIN_RTO / 2;
    private static final int MAX_RTO = 60*1000;
    private static final int CLOCK_SKEW_FUDGE = (ACK_FREQUENCY * 2) / 3;

    /**
     *  @param rtt from the EstablishState, or 0 if not available
     */
    public PeerState(RouterContext ctx, UDPTransport transport,
                     byte[] remoteIP, int remotePort, Hash remotePeer, boolean isInbound, int rtt) {
        _context = ctx;
        _log = ctx.logManager().getLog(PeerState.class);
        _transport = transport;
        long now = ctx.clock().now();
        _keyEstablishedTime = now;
        _lastSendTime = now;
        _lastSendFullyTime = now;
        _lastReceiveTime = now;
        _currentACKs = new LockFreeRingBuffer<Ack>(512);
        _resendACKs = new LockFreeRingBuffer<Ack>(512);
        _sendWindowMessages = DEFAULT_SEND_WINDOW_MESSAGES;
        _sendWindowMessagesRemaining = DEFAULT_SEND_WINDOW_MESSAGES;
        _lastSendRefill = now;
        _remotePort = remotePort;
        if (remoteIP.length == 4) {
            _mtu = LARGE_MTU;
            _mtuReceive = DEFAULT_MTU;
            _largeMTU = transport.getMTU(false);
            _fragmentsize = 1202;
        } else {
            _mtu = MAX_IPV6_MTU;
            _mtuReceive = MIN_IPV6_MTU;
            _largeMTU = transport.getMTU(true);
            _fragmentsize = 1186;
        }
        //_mtuLastChecked = -1;
        _lastACKSend = -1;

        _rto = INIT_RTO;
        _rtt = INIT_RTT;
        _rttDeviation = _rtt * 2;
        if (rtt > 0)  
            recalculateTimeouts(rtt);
            
        _inboundMessages = new ConcurrentLinkedQueue<InboundMessageState>();
        _outboundMessages = new LinkedHashMap<Integer, OutboundMessageState>(256);
        _multiFrag = new ConcurrentLinkedQueue<OutboundMessageState>();
        //_outboundQueue = new CoDelPriorityBlockingQueue(ctx, "UDP-PeerState", 32);
        _outboundQueue = new LockFreeQueue<OutboundMessageState>(512);
        // all createRateStat() moved to EstablishmentManager
        _remoteIP = remoteIP;
        _remotePeer = remotePeer;
        _isInbound = isInbound;
        _remoteHostId = new RemoteHostId(remoteIP, remotePort);
        _nextSendTime = 0;
        _lossEvent = -99;
        _nextInfoTime = now + 30 * 60 * 1000;
    }
    
    public boolean lock() {
        return _lock.compareAndSet(false, true);
    }
    
    public void unlock() {
        _lock.set(false);
    }
    
    /** 
     *  Caller should sync; UDPTransport must remove and add to peersByRemoteHost map
     *  @since 0.9.3
     */
    void changePort(int newPort) {
        if (newPort != _remotePort) {
            _remoteHostId = new RemoteHostId(_remoteIP, newPort);
            _remotePort = newPort;
        }
    }

    /**
     * The peer are we talking to.  This should be set as soon as this
     * state is created if we are initiating a connection, but if we are
     * receiving the connection this will be set only after the connection
     * is established.
     */
    public Hash getRemotePeer() { return _remotePeer; }
    /** 
     * The AES key used to verify packets, set only after the connection is
     * established.  
     */
    SessionKey getCurrentMACKey() { return _currentMACKey; }
    /**
     * The AES key used to encrypt/decrypt packets, set only after the 
     * connection is established.
     */
    SessionKey getCurrentCipherKey() { return _currentCipherKey; }

    /** 
     * The pending AES key for verifying packets if we are rekeying the 
     * connection, or null if we are not in the process of rekeying.
     *
     * @return null always, rekeying unimplemented
     */
    SessionKey getNextMACKey() { return _nextMACKey; }

    /** 
     * The pending AES key for encrypting/decrypting packets if we are 
     * rekeying the connection, or null if we are not in the process 
     * of rekeying.
     *
     * @return null always, rekeying unimplemented
     */
    SessionKey getNextCipherKey() { return _nextCipherKey; }


    /** when were the current cipher and MAC keys established/rekeyed? */
    public long getKeyEstablishedTime() { return _keyEstablishedTime; }

    /**
     *  How far off is the remote peer from our clock, in milliseconds?
     *  A positive number means our clock is ahead of theirs.
     */
    public long getClockSkew() { return _clockSkew ; }

    /** when did we last send them a packet? */
    public long getLastSendTime() { return _lastSendTime; }
    /** when did we last send them a message that was ACKed? */
    public long getLastSendFullyTime() { return _lastSendFullyTime; }
    /** when did we last receive a packet from them? */
    public long getLastReceiveTime() { return _lastReceiveTime; }
    /** unused: how many seconds have we sent packets without any ACKs received? */
    public int getConsecutiveFailedSends() { return 0; }

    /** 
     *  how many messages should we send to the peer in a second
     *  1st stat in CWND column, otherwise unused,
     *  candidate for removal
     */
    public int getSendWindowMessages() { return _sendWindowMessages; }

    /**
     * refills allowed traffic
     * updates stats, send window size and maximum retransmissions
     * based on time elapsed, messages sent and messages acked
     **/
    void updateSendWindow(long now) {
        int duration = (int)(now - _lastSendRefill);
        if (duration < ACK_FREQUENCY) return;
        if (duration < 1000) {
            /** speed: sendrate for 1 sec interval = new traffic + portion of old traffic */
            _sendBps = _sendBytes + ((1000 - duration) * _sendBps + 500) / 1000;
            _receiveBps = _receiveBytes + (_receiveBps * (1000 - duration) + 500) / 1000;
        } else {
            _sendBps = (_sendBytes * 1000 + duration / 2) / duration;
            _receiveBps = (_receiveBytes * 1000 + duration / 2) / duration;
        }
        _sendBytes = 0;
        _receiveBytes = 0;
        _lastSendRefill = now;

        if (_packetsReceived - _lossEvent > 100)
            _throttled = false;
        int tmp = _messagesAcked;
        int diff = tmp - _oldMessagesAcked;
        _oldMessagesAcked = tmp;
        if (duration < SEND_WINDOW) {
            int oldWindowMessages = _sendWindowMessages;
            // adjust send window, remove elapsed period, add 1.125 * messages acked
            // _sendWindowMessages include retransmissions, _messagesAcked do not
            // so window is grown only for constantly sending, low error connections
            _sendWindowMessages = Math.max(MINIMUM_WINDOW_MESSAGES,
                (_sendWindowMessages * (SEND_WINDOW - duration) + SEND_WINDOW / 2) / SEND_WINDOW + (diff * 9 + 4) / 8);
            // adjust for possible changes in send window (= fast run down if no ACKs)
            // retrofit 50% of change
            if (oldWindowMessages != _sendWindowMessages)
                _sendWindowMessagesRemaining = Math.max((_sendWindowMessagesRemaining + 1) / 2,
                _sendWindowMessagesRemaining + (_sendWindowMessages - oldWindowMessages + 1) / 2);
            /** speed: refill remaining */
            _sendWindowMessagesRemaining = Math.min(_sendWindowMessages,
                _sendWindowMessagesRemaining + (_sendWindowMessages * duration + SEND_WINDOW / 2) / SEND_WINDOW);
        } else {
            _sendWindowMessages = Math.max(MINIMUM_WINDOW_MESSAGES, (diff * 9 + 4) / 8);
            _sendWindowMessagesRemaining = _sendWindowMessages;
        }
    }

    /** how many bytes can we send to the peer in the current period */
    public int getSendWindowMessagesRemaining() {
        return _sendWindowMessagesRemaining;
    }

    /** what IP is the peer sending and receiving packets on? */
    public byte[] getRemoteIP() { return _remoteIP; }

    /**
     *  @return may be null if IP is invalid
     */
    public InetAddress getRemoteIPAddress() {
        if (_remoteIPAddress == null) {
            synchronized (this) {
                if (_remoteIPAddress == null)
                    try {
                        _remoteIPAddress = InetAddress.getByAddress(_remoteIP);
                    } catch (UnknownHostException uhe) {
                        if (_log.shouldLog(Log.ERROR))
                            _log.error("Invalid IP? ", uhe);
                    }
            }
        }
        return _remoteIPAddress;
    }

    /** what port is the peer sending and receiving packets on? */
    public int getRemotePort() { return _remotePort; }

    /** 
     * if we are serving as an introducer to them, this is the the tag that
     * they can publish that, when presented to us, will cause us to send
     * a relay introduction to the current peer 
     * @return 0 (no relay) if unset previously
     */
    public long getWeRelayToThemAs() { return _weRelayToThemAs; }

    /**
     * If they have offered to serve as an introducer to us, this is the tag
     * we can use to publish that fact.
     * @return 0 (no relay) if unset previously
     */
    public long getTheyRelayToUsAs() { return _theyRelayToUsAs; }

    /** what is the largest packet we can send to the peer? */
    public int getMTU() { return _mtu; }

    /**
     *  Estimate how large the other side's MTU is.
     *  This could be wrong.
     *  It is used only for the HTML status.
     */
    public int getReceiveMTU() { return _mtuReceive; }

    /** 
     * The AES key used to verify packets, set only after the connection is
     * established.  
     */
    void setCurrentMACKey(SessionKey key) { _currentMACKey = key; }

    /**
     * The AES key used to encrypt/decrypt packets, set only after the 
     * connection is established.
     */
    void setCurrentCipherKey(SessionKey key) { _currentCipherKey = key; }


    /**
     *  Update the moving-average clock skew based on the current difference.
     *  The raw skew will be adjusted for RTT/2 here.
     *  A positive number means our clock is ahead of theirs.
     *  @param skew milliseconds, NOT adjusted for RTT.
     */
    void adjustClockSkew(long skew) { 
        // the real one-way delay is much less than RTT / 2, due to ack delays,
        // so add a fudge factor
        long actualSkew = skew + CLOCK_SKEW_FUDGE - (_rtt / 2); 
        // First time...
        // This is important because we need accurate
        // skews right from the beginning, since the median is taken
        // and fed to the timestamper. Lots of connections only send a few packets.
        if (_packetsReceived <= 1) {
            synchronized(_clockSkewLock) {
                _clockSkew = actualSkew; 
            }
            return;
        }
        double adj = 0.1 * actualSkew; 
        synchronized(_clockSkewLock) {
            _clockSkew = (long) (0.9*_clockSkew + adj); 
        }
    }

    /** when did we last send them a packet? */
    void setLastSendTime(long when) { _lastSendTime = when; }
    /** when did we last receive a packet from them? */
    void setLastReceiveTime(long when) { _lastReceiveTime = when; }

    /**
     *  Note ping sent. Does not update last send time.
     *  @since 0.9.3
     */
    void setLastPingTime(long when) { _lastPingTime = when; }

    /**
     *  Latest of last sent, last ACK, last ping
     *  @since 0.9.3
     */
    long getLastSendOrPingTime() {
        return Math.max(Math.max(_lastSendTime, _lastACKSend), _lastPingTime);
    }

    /**
     * An approximation, for display only
     * @return the smoothed send transfer rate
     */
    public int getSendBps() { return _sendBps; }

    /**
     * An approximation, for display only
     * @return the smoothed receive transfer rate
     */
    public int getReceiveBps() { return _receiveBps; }


    public long getInactivityTime() {
        return _context.clock().now() - Math.max(_lastReceiveTime, _lastSendFullyTime);
    }
    
    
    /** 
     * Decrement the remaining bytes in the current period's window,
     * returning true if the full size can be decremented, false if it
     * cannot.  If it is not decremented, the window size remaining is 
     * not adjusted at all.
     *
     *  Caller should synch
     */

    private boolean allocateSendingBytes(int size) {

        // Ticket 2505
        // We always send all unacked fragments for a message,
        // because we don't have any mechanism in OutboundMessageFragments
        // to track the next send time for fragments individually.
        // Huge messages that are larger than the window size could never
        // get sent and block the outbound queue forever.
        // So we let it through when the window is empty (full window remaining).
        if (_sendWindowMessagesRemaining == 0) return false;
        _sendWindowMessagesRemaining--;
        _sendBytes += size;
        return true;
            //if (isForACK) 
            //    _sendACKBytes += size;
    }
    

    /** 
     * if we are serving as an introducer to them, this is the the tag that
     * they can publish that, when presented to us, will cause us to send
     * a relay introduction to the current peer 
     * @param tag 1 to Integer.MAX_VALUE, or 0 if relaying disabled
     */
    void setWeRelayToThemAs(long tag) { _weRelayToThemAs = tag; }

    /**
     * If they have offered to serve as an introducer to us, this is the tag
     * we can use to publish that fact.
     * @param tag 1 to Integer.MAX_VALUE, or 0 if relaying disabled
     */
    void setTheyRelayToUsAs(long tag) { _theyRelayToUsAs = tag; }


    /**
     *  stat in SST column, otherwise unused,
     *  candidate for removal
     */
    public int getSlowStartThreshold() { return 0; }

    /**
     *  2nd stat in CWND column, otherwise unused,
     *  candidate for removal
     */
    public int getConcurrentSends() { return _outboundMessages.size(); }

    /**
     *  3rd stat in CWND column, otherwise unused,
     *  candidate for removal
     */
    public int getConcurrentSendWindow() { return _outboundQueue.size(); } // hack

    /**
     *  4th stat in CWND column, otherwise unused,
     *  candidate for removal
     */
    public int getConsecutiveSendRejections() { return _retransmissions; } // hack

    public boolean isInbound() { return _isInbound; }

    /** @since IPv6 */
    public boolean isIPv6() {
        return _remoteIP.length == 16;
    }

    /** the last time we used them as an introducer, or 0 */
    long getIntroducerTime() { return _lastIntroducerTime; }

    /** set the last time we used them as an introducer to now */
    void setIntroducerTime() { _lastIntroducerTime = _context.clock().now(); }

    /** 
     *  Do we still need to ACK ? includes partials
     */
    boolean hasACKs() {
        // we are not going to ACK, if there are resend ACKs only: not worth the b/w
        // not queried for piggybacks
        return _hasACKs;
    }

    int countACKs() { return _currentACKs.size() + _resendACKs.size(); }

    public static class Ack {
        public final int messageId;
        public int sends = 0;

        public Ack(int mid) {
            messageId = mid;
        }
    }
    /** 
     *  Queue up a full ACK
     *  @param msg completely received message or 0
     */
    void addACK(int msg) {
        _currentACKs.offer(new Ack(msg));
        _hasACKs = true;
    }

    /** 
     * Get next ACK to send if time due
     * requeue if ACKed first time
     * Side effect - sets _lastACKSend if rv is non-empty
    */
    private Ack _cutOff;
    private volatile boolean _hasACKs;

    Ack getNextACK(long now) {
        Ack ack = _currentACKs.poll();
        if (ack == null) {
            ack = _resendACKs.peek();
            if (ack == null) {
                _hasACKs = false;
                return null;
            }
        }
        if (ack == _cutOff) {
            _hasACKs = _messagesReceived >> 7 <= _packetsReceivedDuplicate; // lots of errors, lets always resend
            return _cutOff = null;
        }
        if (ack.sends == 0) {
            if (_cutOff == null)
                _cutOff = ack;
            ack.sends = 1;
            _resendACKs.offer(ack);
        } else
            _resendACKs.poll();
        if (_log.shouldLog(Log.INFO))
            _log.info("Sent ack " + ack.messageId + " now " + _currentACKs.size() + " current and resend acks");
        _lastACKSend = now;
        /** if this runs dry, we have already cleared out partials first */
        return ack;
    }
    
    /** 
     *  We received the message specified completely.
     *  @param bytes if less than or equal to zero, message is a duplicate.
     */
    void messageFullyReceived(int messageId, int bytes) { messageFullyReceived(messageId, bytes, false); }

    /** 
     *  We received the message specified completely.
     *  @param isForACK unused
     *  @param bytes if less than or equal to zero, message is a duplicate.
     */
    private void messageFullyReceived(int messageId, int bytes, boolean isForACK) {
        addACK(messageId);
        if (bytes <= 0) {
            _packetsReceivedDuplicate++;
            return;
        }
        _receiveBytes += bytes;
            //if (isForACK)
            //    _receiveACKBytes += bytes;
        _messagesReceived++;
    }
        
    void messagePartiallyReceived(int messageId) {
        _hasACKs = true;
    }
    
    /** 
     * Fetch the internal id (Long) to InboundMessageState for incomplete inbound messages.
     * Access to this map must be synchronized explicitly!
     */
    ConcurrentLinkedQueue<InboundMessageState> getInboundMessages() { return _inboundMessages; }

    /**
     * Expire partially received inbound messages, returning how many are still pending.
     * This should probably be fired periodically, in case a peer goes silent and we don't
     * try to send them any messages (and don't receive any messages from them either)
     *
     */
    int expireInboundMessages() { 
        if (_inboundMessages.isEmpty())
            return 0;

        int rv = 0;
        for (Iterator<InboundMessageState> iter = _inboundMessages.iterator(); iter.hasNext(); ) {
            InboundMessageState state = iter.next();
            if (state.isExpired()) {
                iter.remove();
                // state.releaseResources() ??
            } else { // do not do this, possible race
/*                 if (state.isComplete()) {
                    _log.error("inbound message is complete, but wasn't handled inline? " + state + " with " + this);
                    iter.remove();
                    // state.releaseResources() ??
                } else {
 */                    rv++;
            }
        }
        return rv;
    }
    
    /** 
     * grab a list of ACKBitfield instances, some of which may fully 
     * ACK a message while others may only partially ACK a message.  
     * the values returned are limited in size so that they will fit within
     * the peer's current MTU as an ACK - as such, not all messages may be
     * ACKed with this call.  Be sure to check getWantedACKSendSince() which
     * will be unchanged if there are ACKs remaining.
     *
     * @return non-null, possibly empty
     * @deprecated unused
     */
    @Deprecated
    List<ACKBitfield> retrieveACKBitfields() { return retrieveACKBitfields(true); }

    /**
     * See above. Only called by ACKSender with alwaysIncludeRetransmissions = false.
     * So this is only for ACK-only packets, so all the size limiting is useless.
     * FIXME.
     *
     * @return non-null, possibly empty
     */
    List<ACKBitfield> retrieveACKBitfields(boolean alwaysIncludeRetransmissions) {
        if (_inboundMessages.isEmpty()) 
            return null;

        List<ACKBitfield> partial = fetchPartialACKs();
        if (partial == null)
            return null;

        int bytesRemaining = countMaxACKData();
        List<ACKBitfield> rv = null;

        // we may not be able to use them all, but lets try...
        for (int i = 0; (bytesRemaining > 4) && (i < partial.size()); i++) {
            ACKBitfield bitfield = partial.get(i);
            int bytes = (bitfield.fragmentCount() / 7) + 1;
            if (bytesRemaining > bytes + 4) { // msgId + bitfields
                if (rv == null)
                    rv = new ArrayList<ACKBitfield>();
                rv.add(bitfield);
                bytesRemaining -= bytes + 4;
            } else {
                // continue on to another partial, in case there's a 
                // smaller one that will fit
            }
        }
        return rv;
    }
    
    /**
     *  @param rv out parameter, populated with true partial ACKBitfields.
     *            no full bitfields are included.
     */
    List<ACKBitfield> fetchPartialACKs() {
        if (_inboundMessages.isEmpty()) return null;
        List<ACKBitfield> rv = null;
        for (Iterator<InboundMessageState> iter = _inboundMessages.iterator(); iter.hasNext(); ) {
            InboundMessageState state = iter.next();
            if (state.isExpired()) {
                //if (_context instanceof RouterContext)
                //    ((RouterContext)_context).messageHistory().droppedInboundMessage(state.getMessageId(), state.getFrom(), "expired partially received: " + state.toString());
                iter.remove();
                continue;
                // state.releaseResources() ??
            }
            if (state.isComplete()) continue;
            if (rv == null)
                rv = new ArrayList<ACKBitfield>();
            rv.add(state.createACKBitfield());
        }
        return rv;
    }

    /** This is the value specified in RFC 2988 */
    //    private static final float RTT_DAMPENING = 0.125f;
    private static final int RTT_DAMPENING_INVERSE = 8;
    
    /**
     *  Adjust the tcp-esque timeouts.
     *  Caller should synch on this
     */
    private void recalculateTimeouts(int lifetime) {
        int diff = lifetime - _rtt;
            // the rttDev calculation matches that recommended in RFC 2988 (beta = 1/4)
        // speed: converted previous complicated math to integer equivalent
        // speed: we maintain the fourfold _rttDeviation for simpler calcs
        _rttDeviation = (3 * _rttDeviation + 2) / 4 + Math.abs(diff);
        _rtt = (_rtt * RTT_DAMPENING_INVERSE + diff + RTT_DAMPENING_INVERSE / 2) / RTT_DAMPENING_INVERSE;
        // K = 4
        _rto = Math.min(MAX_RTO, Math.max(MIN_RTO, _rtt + _rttDeviation));
        //if (_log.shouldLog(Log.DEBUG))
        //    _log.debug("Recalculating timeouts w/ lifetime=" + lifetime + ": rtt=" + _rtt
        //               + " rttDev=" + _rttDeviation + " rto=" + _rto);
    }
    
    /**
     *  Caller should synch on this
     *
    private void adjustMTU() {
        if (_packetsTransmitted > 10) {
            // speed: replace expensive float math
            // allow large MTU if retransPct < 25%
            boolean wantLarge = _packetsRetransmitted * 4 / _packetsTransmitted == 0; // heuristic to allow fairly lossy links to use large MTUs
            if (wantLarge && _mtu != _largeMTU) {
                if (_context.random().nextLong(_mtuDecreases) <= 0) {
                    _mtu = _largeMTU;
                    _mtuIncreases++;
		}
	    } else if (!wantLarge && _mtu == _largeMTU) {
                _mtu = _remoteIP.length == 4 ? MIN_MTU : MIN_IPV6_MTU;
                _mtuDecreases++;
	    }
        } else {
            _mtu = _remoteIP.length == 4 ? DEFAULT_MTU : MIN_IPV6_MTU;
        }
    }
    */

    /**
     *  @since 0.9.2
     */
    void setHisMTU(int mtu) {
        if (mtu <= MIN_MTU || mtu >= _largeMTU ||
            (_remoteIP.length == 16 && mtu <= MIN_IPV6_MTU))
            return;
        _largeMTU = mtu;
        if (mtu < _mtu)
            _mtu = mtu;
    }
    
    /** we are resending a packet, so lets jack up the rto */
    void messageRetransmitted(int packets) {
        _packetsRetransmitted += packets;
        // adjustMTU(); // either we time out or do it after ACK
    }

    void packetsTransmitted(int packets) { 
        _packetsTransmitted += packets; 
    }

    /** how long does it usually take to get a message ACKed? */
    public int getRTT() { return _rtt; }
    /** how soon should we retransmit an unacked packet? */
    public int getRTO() { return _rto; }
    
    /**
     *  I2NP messages sent.
     *  Does not include duplicates.
     *  As of 0.9.24, incremented when bandwidth is allocated just before sending, not when acked.
     */
    public int getMessagesSent() { return _messagesSent; }
    
    /**
     *  I2NP messages received.
     *  As of 0.9.24, does not include duplicates.
     */
    public int getMessagesReceived() { return _messagesReceived; }

    public int getPacketsTransmitted() { return _packetsTransmitted; }
    public int getPacketsRetransmitted() { return _packetsRetransmitted; }
    //public long getPacketsPeriodTransmitted() { return _packetsPeriodTransmitted; }
    //public int getPacketsPeriodRetransmitted() { return _packetsPeriodRetransmitted; }

    public int getPacketsReceived() { return _packetsReceived; }
    public int getPacketsReceivedDuplicate() { return _packetsReceivedDuplicate; }

    private static final int MTU_RCV_DISPLAY_THRESHOLD = 20;
    /** 60 */
    private static final int OVERHEAD_SIZE = PacketBuilder.IP_HEADER_SIZE + PacketBuilder.UDP_HEADER_SIZE +
                                             UDPPacket.MAC_SIZE + UDPPacket.IV_SIZE;
    /** 80 */
    private static final int IPV6_OVERHEAD_SIZE = PacketBuilder.IPV6_HEADER_SIZE + PacketBuilder.UDP_HEADER_SIZE +
                                             UDPPacket.MAC_SIZE + UDPPacket.IV_SIZE;

    /** 
     *  @param size not including IP header, UDP header, MAC or IV
     */
    void packetReceived(int size) { 
        _packetsReceived++; 
        int minMTU;
        boolean ipv4 = _remoteIP.length == 4;
        int overhead;
        if (ipv4) {
            minMTU = MIN_MTU;
            overhead = OVERHEAD_SIZE;
        } else {
            minMTU = MIN_IPV6_MTU;
            overhead = IPV6_OVERHEAD_SIZE;
        }
        size += overhead;
        if (size <= minMTU) {
            _consecutiveSmall++;
            if (_consecutiveSmall >= MTU_RCV_DISPLAY_THRESHOLD)
                _mtuReceive = minMTU;
        } else {
            _consecutiveSmall = 0;
            if (size > _mtuReceive) {
                _mtuReceive = size;
                if (size > _mtu) {
                    if (ipv4)
                        _mtu = ((size + 4) & 0xff0) - 4;
                    else
                        _mtu = size & 0xff0;
                    int maxPayload = _mtu - overhead
                        - PacketBuilder.DATA_HEADER_SIZE + UDPPacket.MAC_SIZE + UDPPacket.IV_SIZE;
                    if (_fragmentsize < maxPayload)
                        _fragmentsize = maxPayload;
                }
            }
        }
    }
    
    void dataReceived() {
        _lastReceiveTime = _context.clock().now();
    }
    
    /** when did we last send an ACK to the peer? */
    public long getLastACKSend() { return _lastACKSend; }

    /** @deprecated unused */
    @Deprecated
    public void setLastACKSend(long when) { _lastACKSend = when; }


    /**
     *  Are we out of room to send all the current unsent acks in a single packet?
     *  This is a huge threshold (134 for small MTU and 255 for large MTU)
     *  that is rarely if ever exceeded in practice.
     *  So just use a fixed threshold of half the resend acks, so that if the
     *  packet is lost the acks have a decent chance of getting retransmitted.
     *  Used only by ACKSender.
     */
    boolean unsentACKThresholdReached() {
        // ACK after possibly 1/3 of send window received
        return _currentACKs.size() > DEFAULT_SEND_WINDOW_MESSAGES / 3;
    }

    /**
     *  @return how many bytes available for acks in an ack-only packet, == MTU - 83
     *          Max of 1020
     */
    private int countMaxACKData() {
        return Math.min(PacketBuilder.ABSOLUTE_MAX_ACKS * 4,
                _mtu 
                - (_remoteIP.length == 4 ? PacketBuilder.IP_HEADER_SIZE : PacketBuilder.IPV6_HEADER_SIZE)
                - PacketBuilder.UDP_HEADER_SIZE
                - UDPPacket.IV_SIZE 
                - UDPPacket.MAC_SIZE
                - 1 // type flag
                - 4 // timestamp
                - 1 // data flag
                - 1 // # ACKs
                - 16); // padding safety
    }
    
    /** @return non-null */
    RemoteHostId getRemoteHostId() { return _remoteHostId; }
    
    /**
     *  TODO should this use a queue, separate from the list of msgs pending an ack?
     *  TODO bring back tail drop?
     *  TODO priority queue? (we don't implement priorities in SSU now)
     *  TODO backlog / pushback / block instead of dropping? Can't really block here.
     *  TODO SSU does not support isBacklogged() now
     */
    void add(OutboundMessageState state) {
        if (state.getPeer() != this) {
            if (_log.shouldLog(Log.WARN))
                _log.warn("Not for me!", new Exception("I did it"));
            _transport.failed(state, false);
            return;
	}
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("Adding to " + _remotePeer + ": " + state.getMessageId());
        // will never fail for CDPQ
        boolean fail;
        fail = !_outboundQueue.offer(state);
/****
        synchronized (_outboundMessages) {
            rv = _outboundMessages.size() + 1;
            if (rv > MAX_SEND_MSGS_PENDING) { 
                // too many queued messages to one peer?  nuh uh.
                fail = true;
                rv--;
****/

         /******* proactive tail drop disabled by jr 2006-04-19 so all this is pointless

            } else if (_retransmitter != null) {
                long lifetime = _retransmitter.getLifetime();
                long totalLifetime = lifetime;
                for (int i = 1; i < msgs.size(); i++) { // skip the first, as thats the retransmitter
                    OutboundMessageState cur = msgs.get(i);
                    totalLifetime += cur.getLifetime();
                }
                long remaining = -1;
                OutNetMessage omsg = state.getMessage();
                if (omsg != null)
                    remaining = omsg.getExpiration() - _context.clock().now();
                else
                    remaining = 10*1000 - state.getLifetime();
                
                if (remaining <= 0)
                    remaining = 1; // total lifetime will exceed it anyway, guaranteeing failure
                float pDrop = totalLifetime / (float)remaining;
                pDrop = pDrop * pDrop * pDrop;
                if (false && (pDrop >= _context.random().nextFloat())) {
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Proactively tail dropping for " + _remotePeer.toBase64() + " (messages=" + msgs.size() 
                                  + " headLifetime=" + lifetime + " totalLifetime=" + totalLifetime + " curLifetime=" + state.getLifetime() 
                                  + " remaining=" + remaining + " pDrop=" + pDrop + ")");
                    _context.statManager().addRateData("udp.queueDropSize", msgs.size(), totalLifetime);
                    fail = true;
                } else { 
                    if (_log.shouldLog(Log.DEBUG))
                        _log.debug("Probabalistically allowing for " + _remotePeer.toBase64() + " (messages=" + msgs.size() 
                                   + " headLifetime=" + lifetime + " totalLifetime=" + totalLifetime + " curLifetime=" + state.getLifetime() 
                                   + " remaining=" + remaining + " pDrop=" + pDrop + ")");
                    _context.statManager().addRateData("udp.queueAllowTotalLifetime", totalLifetime, lifetime);
                    msgs.add(state);
                }

             *******/
/****
            } else {
                _outboundMessages.add(state);
            }
        }
****/
        if (fail) {
            if (_log.shouldLog(Log.WARN))
                _log.warn("Dropping msg, OB queue full for " + toString());
            _transport.failed(state, false);
        }
    }

    /** drop all outbound messages */
    void dropOutbound() {
        //if (_dead) return;
        //_outboundMessages = null;
        List<OutboundMessageState> failList = new ArrayList<OutboundMessageState>(128);
        synchronized (_outboundMessages) {
            for (Map.Entry<Integer, OutboundMessageState> me : _outboundMessages.entrySet())
                failList.add(me.getValue());
            _outboundMessages.clear();
        }
        _multiFrag.clear();
        _outboundQueue.drainTo(failList);
        _currentACKs.clear();
        _resendACKs.clear();
        for (OutboundMessageState failState : failList)
            _transport.failed(failState, false);
    }
    
    /**
     * @return Do we have something queued up?
     */
    public boolean noQueue() {
        return _outboundQueue.isEmpty();
    }

    /**
     * @return do we have active outbound messages remaining (unsynchronized)
     */
    public boolean hasOutBound() {
        return _outboundMessages.size() > 0 || !noQueue();
    }

    /**
     * Sets to true.
     * @since 0.9.24
     */
    public void setMayDisconnect() { _mayDisconnect = true; }

    /**
     * @since 0.9.24
     */
    public boolean getMayDisconnect() { return _mayDisconnect; }

    /**
     * Expire any outbound messages
     *
     * @return do we have no outbound messages remaining?
     */
    public boolean finishMessages(long now) {
        if (_oldestInPlay != null && _oldestInPlay.isExpired(now)) {
            boolean didRemove;
            synchronized (_outboundMessages) {
                didRemove = _outboundMessages.remove(_oldestInPlay.getMessageId()) != null;
            }
            if (didRemove) {
                if (_oldestInPlay.getFragmentCount() > 1)
                    _multiFrag.remove(_oldestInPlay);
                if (_oldestInPlay.getMessage() != null) {
                    _transport.failed(_oldestInPlay);
                    if (_log.shouldWarn())
                        _log.warn("Message expired: " + _oldestInPlay + " to: " + this);
                } else {
                    // it can not have an OutNetMessage if the source is the
                    // final after establishment message
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Unable to send a direct message: " + _oldestInPlay);
                }
                _oldestInPlay = null;
            }
        }
        if (_outboundMessages.isEmpty() && _outboundQueue.isEmpty())
            if (now >= _nextInfoTime) {
                _nextInfoTime = now + 30 * 60 * 1000;
                _transport.getEstablisher().sendOurInfo(this);
            } else {
                setNextSendTime(0);
                expireInboundMessages();
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("No more pending messages for " + getRemotePeer());
                _retransmissions = 0;
                return true;
            }
        return false;
    }

    /**
     * strip out all ACKed messages
     *
     * @return retransmitters not stripped if any
     * timeouts take precedence over fast retransmit
     */
    private int checkResends(OutboundMessageState[] states, long now) {
        int retransmissions = 0;
        int rv = 0;
        try {
            // shortcut typical case: nothing pending
            if (_outboundMessages.isEmpty()) return 0;
            OutboundMessageState state;
            boolean due;
            synchronized (_outboundMessages) {
                if (_outboundMessages.isEmpty()) return 0;
                Iterator<Map.Entry<Integer, OutboundMessageState>> iter = _outboundMessages.entrySet().iterator();
                state = iter.next().getValue();
                if (_oldestInPlay != state) {
                    _oldestInPlay = state;
                    // reset, if: first round or oldest timed out or
                    // oldest message was acked, reset timer
                    _retransmitTimer = state.getStartedOn() + _rto;
                }
                due = now > _retransmitTimer;
                long fastRetransCutoff = now;
                int maxAnswerTime = _rtt + _rttDeviation + ACK_FREQUENCY;
                if (_throttled)
                    fastRetransCutoff -= Math.max(maxAnswerTime, MIN_RTO); // no fast retransmits
                else
                    fastRetransCutoff -= Math.min(maxAnswerTime, MIN_RTO);
                do {
                    if (state.getPushCount() > 1) { // this one is retransmitting
                        retransmissions++;
                        // brute force: if the oldest times out, resend all retransmitters
                        if (due) states[rv++] = state;
                    } else {
                        long start = state.getStartedOn();
                        if (start > fastRetransCutoff) break;  // later messages must not retransmit
                        // here: due first time rtx plus possible fast rtx
                        if (start >= _lastSendFullyTime && !due) break; // later messages must not retransmit
                        states[rv++] = state;
                        retransmissions++;
                    }
                    if (!iter.hasNext()) break;
                    state = iter.next().getValue();
                } while (rv < PacketBuilder.ABSOLUTE_MAX_ACKS);
            }
            if (due) {
                // timed out, so increase timeout
                _rto = Math.min(MAX_RTO, _rto << 1);
                _retransmitTimer = now + _rto;
            }
        } finally { _retransmissions = retransmissions; }
        return rv;
    }
    
    public void addOutbound(OutboundMessageState state) {
        synchronized (_outboundMessages) {
            _outboundMessages.put(state.getMessageId(), state);
        }
        if (state.getFragmentCount() > 1)
            _multiFrag.add(state);
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("Allocate sending (NEW) to " + _remotePeer + ": " + state.getMessageId());
}

    /**
     * Pick one or more messages we want to send and allocate them out of our window
     *
     * @return allocated messages to send (never empty), or null if no messages or no resources
     */
    int allocateSend(OutboundMessageState[] states, long now) {
        int rv = 0;
        if (now >= _resendCheck) {
            rv = checkResends(states, now); // returns retransmitters, if any
            if (rv >= Math.max(2, _sendWindowMessages >> 4)) {
                if (_packetsReceived - _lossEvent <= 40)
                    _throttled = true;
                _lossEvent = _packetsReceived;
            }
            updateSendWindow(now); // run down window size early if no ACKs have come in
            _resendCheck = now + ACK_FREQUENCY;
            for (int i = 0; i < rv; i++) {
                if (!shouldSend(states[i])) {
                    if (i > 0) _lastSendTime = now;
                    return i;
                }
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("Allocate sending (OLD) to " + _remotePeer + ": " + states[i].getMessageId());
            }
        }
        // Peek at head of _outboundQueue and see if we can send it.
        // If so, pull it off, put it in _outbundMessages, test
        // again for bandwidth if necessary, and return it.
        OutboundMessageState state;
        while ((state = _outboundQueue.poll()) != null && state.isExpired(now))
            _transport.failed(state);
        if (state != null) {
            int allowed = PacketBuilder.ABSOLUTE_MAX_ACKS - rv;
            if (allowed > 0 && shouldSend(state)) {
                synchronized (_outboundMessages) {
                    do {
                        states[rv++] = state;
                        addOutbound(state);
                        state = null;
                    } while (--allowed > 0 && (state = _outboundQueue.poll()) != null && shouldSend(state));
                }
            }
        }
        if (state != null) _outboundQueue.offer(state);
        if (rv == 0) {
            if (_log.shouldLog(Log.DEBUG))
            _log.debug("Nothing to send to " + _remotePeer + ", with " + _outboundMessages.size() +
                " / " + _outboundQueue.size() + " remaining");
        } else
            _lastSendTime = now;
        return rv;
    }
    
    /**
     *  High usage
     */
    public long getNextSendTime() { return _nextSendTime; }

    /**
     *  When will we need to revisit the peer ?
     */
    void setNextSendTime(long when) {
        // batch together all rechecks every 50 ms
        long remainder = when % 50;
        if (remainder == 0)
            _nextSendTime = when;
        else
            _nextSendTime = when + 50 - remainder;
    }

    /**
     *  @since 0.9.3
     */
    public boolean isBacklogged() {
        return _throttled || _outboundQueue.isBacklogged();
//        return _outboundQueue.isBacklogged();
    }

    public OutboundMessageState getNextState() {
        return _outboundQueue.poll();
    }

    /**
     *  Always leave room for this many explicit acks.
     *  Only for data packets. Does not affect ack-only packets.
     *  This directly affects data packet overhead, adjust with care.
     */
    private static final int MIN_EXPLICIT_ACKS = 3;
    /** this is room for three explicit acks or two partial acks or one of each = 13 */
    private static final int MIN_ACK_SIZE = 1 + (4 * MIN_EXPLICIT_ACKS);

    /**
     *  how much payload data can we shove in there?
     *  @return MTU - 87, i.e. 533 or 1397 (IPv4), MTU - 107 (IPv6)
     */
    int fragmentSize() {
        // 46 + 20 + 8 + 13 = 74 + 13 = 87 (IPv4)
        // 46 + 40 + 8 + 13 = 94 + 13 = 107 (IPv6)
        /** 1488 - 94 = 1394 
         * fits IPv4 and IPv6
         * solves loadFrom() problem in most cases
        */
        return _fragmentsize; // max payload that fits 1280 without padding
//        return _mtu - (_remoteIP.length == 4 ? PacketBuilder.MIN_DATA_PACKET_OVERHEAD : PacketBuilder.MIN_IPV6_DATA_PACKET_OVERHEAD);
//        return _mtu -
//               (_remoteIP.length == 4 ? PacketBuilder.MIN_DATA_PACKET_OVERHEAD : PacketBuilder.MIN_IPV6_DATA_PACKET_OVERHEAD)
               /** speed: do not include ACKs by default. we put them first in preparePackets
                * helps 4K+ messages
                */
                // - MIN_ACK_SIZE
    }
    

    /**
     *  Do we have bandwidth for this?
     * set number of retransmissions and last send time
     */
    public boolean shouldSend(OutboundMessageState state) {
        int size = state.getUnackedSize();
        if (allocateSendingBytes(size)) {
            if (_log.shouldLog(Log.DEBUG))
                    _log.debug("Allocation of " + size + " allowed with " 
                          + _sendWindowMessagesRemaining
                          + "/" + _sendWindowMessages
                              + " remaining"
                              + " for message " + state.getMessageId() + ": " + state);

            if (state.push())
                _messagesSent++;
        
            // no longer extend rto for messages with multiple fragments
            //if (peer.getSendWindowBytesRemaining() > 0)
            //    _throttle.unchoke(peer.getRemotePeer());
            return true;
        }
                if (_log.shouldLog(Log.INFO))
            _log.info("Allocation of " + size + " rejected w/ wsize=" + _sendWindowMessages
                      + " available=" + _sendWindowMessagesRemaining
                              + " for message " + state.getMessageId() + ": " + state);
        if (_log.shouldLog(Log.INFO))
            _log.info("Transmit after choke");
                //_throttle.choke(peer.getRemotePeer());

                //if (state.getMessage() != null)
                //    state.getMessage().timestamp("choked, not enough available, wsize=" 
                //                                 + getSendWindowBytes() + " available="
                //                                 + getSendWindowBytesRemaining());
        return false;
    }
    
    /**
     *  A full ACK was received.
     *  caller must sync
     *
     *  @return the message that was acked
     */
    OutboundMessageState acked(int messageId) {
        // this does not prevent a resend if the packet pusher is beyond stripAckedMessages()
        OutboundMessageState state;
        if ((state = _outboundMessages.remove(messageId)) != null) {
            if (state.getFragmentCount() > 1)
                _multiFrag.remove(state);
            long start = state.getStartedOn();
            int lifetime = (int)(_context.clock().now() - start);
            _lastSendFullyTime = Math.max(start, _lastSendFullyTime);
            _messagesAcked++;
            int numSends = state.getMaxSends();
            if (_log.shouldLog(Log.INFO))
                _log.info("Received ack of " + state.getMessageId() + " by " + _remotePeer
                    + " after " + lifetime + " and " + numSends + " sends" + " and size=" + state.getMessageSize());

            if (numSends == 1)
                // this adjusts the rtt/rto/window/etc
                recalculateTimeouts(lifetime);
                // adjustMTU();
        }
        return state;
    }
    
    /**
     *  A partial ACK was received. This is much less common than full ACKs.
     *  caller must sync
     *
     *  @return true if the message was completely acked for the first time
     */
    OutboundMessageState acked(ACKBitfield bitfield) {
        if (_multiFrag.isEmpty()) return null;
        final int messageId = bitfield.getMessageId();
        for (OutboundMessageState state : _multiFrag) {
            if (state.getMessageId() == messageId) {
                boolean isComplete = state.acked(bitfield);
                try {
                    if (isComplete) 
                        return acked(messageId);
                } finally {
                    if (_log.shouldLog(Log.INFO))
                        _log.info("Received partial ack of " + messageId + " by " + _remotePeer
                            + " after " + state.getLifetime() + " and " + state.getMaxSends() + " sends: " + bitfield + ": completely removed? "
                            + isComplete + ": " + state);
                }
                return null;
            }
        }
        
            // dupack
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("Received an ACK for a message not pending: " + bitfield);
        return null;
    }
    
    /**
     * Transfer the basic activity/state from the old peer to the current peer
     *
     * @param oldPeer non-null
     */
    void loadFrom(PeerState oldPeer) {
        
        // do not transfer all those stats. Will be useless anyway
        
        // do not transfer inbound messages nor ACKs. Will be expired anyway
        synchronized (oldPeer._outboundMessages) {
            synchronized (_outboundMessages) {
                oldPeer._outboundQueue.drainTo(_outboundQueue);
                _outboundMessages.putAll(oldPeer._outboundMessages);
                oldPeer._outboundMessages.clear();
                _multiFrag.addAll(oldPeer._multiFrag);
                oldPeer._multiFrag.clear();
            }
        }
    }

    /**
     *  Convenience for OutboundMessageState so it can fail itself
     *  @since 0.9.3
     */
    UDPTransport getTransport() {
        return _transport;
    }

    // why removed? Some risk of dups in OutboundMessageFragments._activePeers ???

    /*
    public int hashCode() {
        if (_remotePeer != null) 
            return _remotePeer.hashCode();
        else 
            return super.hashCode();
    }
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null) return false;
        if (o instanceof PeerState) {
            PeerState s = (PeerState)o;
            if (_remotePeer == null)
                return o == this;
            else
                return _remotePeer.equals(s.getRemotePeer());
        } else {
            return false;
        }
    }
    */
    
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(256);
        buf.append(_remoteHostId.toString());
        if (_remotePeer != null)
            buf.append(" ").append(_remotePeer.toBase64().substring(0,6));

        buf.append(_isInbound? " IB " : " OB ");
        long now = _context.clock().now();
        buf.append(" recvAge: ").append(now-_lastReceiveTime);
        buf.append(" sendAge: ").append(now-_lastSendFullyTime);
        buf.append(" sendAttemptAge: ").append(now-_lastSendTime);
        buf.append(" sendACKAge: ").append(now-_lastACKSend);
        buf.append(" lifetime: ").append(now-_keyEstablishedTime);
        buf.append(" cwin: ").append(_sendWindowMessages);
        buf.append(" acwin: ").append(_sendWindowMessagesRemaining);
        buf.append(" msgs rcvd: ").append(_messagesReceived);
        buf.append(" msgs sent: ").append(_messagesSent);
        buf.append(" pkts rcvd OK/Dup: ").append(_packetsReceived).append('/').append(_packetsReceivedDuplicate);
        buf.append(" pkts sent OK/Dup: ").append(_packetsTransmitted).append('/').append(_packetsRetransmitted);
        buf.append(" IBM: ").append(_inboundMessages.size());
        buf.append(" OBQ: ").append(_outboundQueue.size());
        buf.append(" OBL: ").append(_outboundMessages.size());
        return buf.toString();
    }
}
