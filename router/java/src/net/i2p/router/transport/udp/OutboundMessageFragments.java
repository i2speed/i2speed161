package net.i2p.router.transport.udp;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.i2p.data.DataHelper;
import net.i2p.data.Hash;
import net.i2p.data.router.RouterInfo;
import net.i2p.router.OutNetMessage;
import net.i2p.router.RouterContext;
import net.i2p.router.transport.udp.PacketBuilder.Fragment;
import net.i2p.util.LockFreeQueue;
import net.i2p.util.LockFreeRingBuffer;
import net.i2p.util.Log;

/**
 * Coordinate the outbound fragments and select the next one to be built.
 * This pool contains messages we are actively trying to send, essentially
 * doing a round robin across each message to send one fragment, as implemented
 * in {@link #getNextVolley()}.  This also honors per-peer throttling, taking
 * note of each peer's allocations.  If a message has each of its fragments
 * sent more than a certain number of times, it is failed out.  In addition,
 * this instance also receives notification of message ACKs from the
 * {@link InboundMessageFragments}, signaling that we can stop sending a
 * message.
 *
 */
class OutboundMessageFragments {
    private final RouterContext _context;
    private final Log _log;
    private final UDPTransport _transport;
    // private ActiveThrottle _throttle; // LINT not used ??

    /**
     *  Peers we are actively sending messages to.
     */
    private final LockFreeQueue<PeerState> _senders;
    private final LockFreeRingBuffer<PeerState> _peersToACK;
    private final LockFreeRingBuffer<PeerState> _activePeers;
    private OutboundMessageState[] _states;
    private Fragment[] _fragments;
    private final PacketBuilder _builder;

    /** if we can handle more messages explicitly, set this to true */
    // private boolean _allowExcess; // LINT not used??
    // private volatile long _packetsRetransmitted; // LINT not used??

    // private static final int MAX_ACTIVE = 64; // not used.

    // gets rounded to next 50 ms
    static final int LOOP_FREQUENCY = PeerState.ACK_FREQUENCY - 49;

    public OutboundMessageFragments(RouterContext ctx, UDPTransport transport, ActiveThrottle throttle) {
        _context = ctx;
        _log = ctx.logManager().getLog(OutboundMessageFragments.class);
        _transport = transport;
        // _throttle = throttle;
        _activePeers = new LockFreeRingBuffer<PeerState>(16, 512);
        _senders = new LockFreeQueue<PeerState>(64);
        _peersToACK = new LockFreeRingBuffer<PeerState>(128);
        allocateTemp();
        _builder = transport.getBuilder();
        // _allowExcess = false;
        _context.statManager().createRequiredRateStat("udp.packetsRetransmitted", "Lifetime of packets during retransmission (ms)", "udp", UDPTransport.RATES);
    }

    public synchronized void startup() {}

    public synchronized void shutdown() {
        _senders.abort();
        _peersToACK.clear();
        _activePeers.clear();
    }

    private void allocateTemp() {
        _states = new OutboundMessageState[PacketBuilder.ABSOLUTE_MAX_ACKS];
        _fragments = new Fragment[64];
    }

    void dropPeer(PeerState peer) {
        if (_log.shouldDebug())
            _log.debug("Dropping peer " + peer.getRemotePeer());
        // @speed
        // no need for concurrent removal
        // we do it inline as dropOutbound removes all
        // last _nextSendTime value can not change any more
        // this will bring him to top of list and finishMessages() kicks him out
        //_activePeers.remove(peer);
        // @speed \\
    }

    /**
     * Block until we allow more messages to be admitted to the active
     * pool.  This is called by the {@link OutboundRefiller}
     *
     * @return true if more messages are allowed
     */
    public boolean waitForMoreAllowed() {
        // test without choking.
        // perhaps this should check the lifetime of the first activeMessage?
        if (true) return true;
        /*

        long start = _context.clock().now();
        int numActive = 0;
        int maxActive = Math.max(_transport.countActivePeers(), MAX_ACTIVE);
        while (_alive) {
            finishMessages();
            try {
                synchronized (_activeMessages) {
                    numActive = _activeMessages.size();
                    if (!_alive)
                        return false;
                    else if (numActive < maxActive)
                        return true;
                    else if (_allowExcess)
                        return true;
                    else
                        _activeMessages.wait(1000);
                }
                _context.statManager().addRateData("udp.activeDelay", numActive, _context.clock().now() - start);
            } catch (InterruptedException ie) {}
        }
         */
        return false;
    }

    /**
     * Add a new message to the active pool
     *
     */
    public void add(OutNetMessage msg) {
        RouterInfo target = msg.getTarget();
        if (target == null)
            return;

        PeerState peer = _transport.getPeerState(target.getIdentity().calculateHash());
        try {
            // will throw NPE if peer == null
            boolean locked = false;
            OutboundMessageState state = new OutboundMessageState(_context, msg, peer);
            if (peer.isBacklogged() || !(locked = peer.lock()) || peer.getNextSendTime() == 0 || !peer.shouldSend(state)) {
                if (locked) peer.unlock();
                boolean noQueue = peer.noQueue();
                peer.add(state);
                if (noQueue) add(peer);
                return;
            }
            try {
                do {
                    peer.addOutbound(state);
                    preparePacketsFast(state, peer);
                    state = peer.getNextState();
                    if (state != null && !peer.shouldSend(state)) {
                        peer.add(state);
                        state = null;
                    } 
                } while (state != null);
            } finally {
                peer.unlock();
            }
        } catch (NullPointerException npe) {
            _transport.failed(msg, "Peer disconnected quickly");
        }
    }

    /**
     *  Short circuit the OutNetMessage, letting us send the establish
     *  complete message reliably.
     *  If you have multiple messages, use the list variant,
     *  so the messages may be bundled efficiently.
     */
    public void add(OutboundMessageState state, PeerState peer) {
        boolean noQueue = peer.noQueue();
        peer.add(state);
        if (noQueue) add(peer);
        //_context.statManager().addRateData("udp.outboundActiveCount", active, 0);
    }

    /**
     *  Short circuit the OutNetMessage, letting us send multiple messages
     *  reliably and efficiently.
     *  @since 0.9.24
     */
    public void add(List<OutboundMessageState> states, PeerState peer) {
        boolean noQueue = peer.noQueue();
        int sz = states.size();
        for (int i = 0; i < sz; i++) {
            OutboundMessageState state = states.get(i);
            peer.add(state);
        }
        if (noQueue) add(peer);
        //_context.statManager().addRateData("udp.outboundActiveCount", active, 0);
    }

    /**
     * Add the peer to the list of peers wanting to transmit something.
     * This wakes up the packet pusher if it is sleeping.
     *
     * Avoid synchronization where possible.
     * There are small chances of races.
     * There are larger chances of adding the PeerState "behind" where
     * the iterator is now... but these issues are the same as before concurrentification.
     *
     * @param size the minimum size we can send, or 0 to always notify
     * @since 0.8.9
     */
    public void add(PeerState peer) {
        if (_senders.offer(peer)) {
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("Add a new message to peer " + peer.getRemotePeer());
        } else {
            if (_log.shouldLog(Log.WARN))
                _log.warn("Buffer overrun - peer not added: " + peer.getRemotePeer());
        }
    }

    /** add a peer we need to ACK
     */
    void ackPeer(PeerState peer) {
        if (peer.hasACKs()) return;
        if (_peersToACK.offer(peer)) {
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("Adding peer to ACK " + peer.getRemotePeer());
         } else {
            if (_log.shouldLog(Log.WARN))
                _log.debug("Buffer overrun - ACK not added: " + peer.getRemotePeer());
        }
    }

    private void requeuePeer(PeerState peer, long now) {
        _activePeers.rotate();
        peer.setNextSendTime(now + LOOP_FREQUENCY);
    }

    private void queuePeer(PeerState peer, long when) {
        if (peer.getNextSendTime() > 0) return;
        _activePeers.offer(peer);
        peer.setNextSendTime(when);
    }
 
    private boolean sendFast(PeerState peer, long now) {
        queuePeer(peer, now + LOOP_FREQUENCY);
        if (peer.isBacklogged() || !peer.lock()) return false;
        try {
            int stateCount;
            if ((stateCount = peer.allocateSend(_states, now)) > 0) {
                preparePackets(stateCount, peer);
                return true;
            }
        } finally {
            peer.unlock();
        }
        return false;
    }

    /**
     * Fetch all the packets for a message volley, blocking until there is a
     * message which can be fully transmitted (or the transport is shut down).
     * Also does (partial) retransmissions and ACKs
     * NOT thread-safe. Called by the PacketPusher thread only.
     */
    public void getNextVolley() {
        PeerState peer;
        long now = _context.clock().now();
        // Step 1 : slip in fresh traffic and send if not busy
        while ((peer = _senders.poll()) != null)
            if (sendFast(peer, now)) return;
        long nextSendTime = 0;
        // Step 2: try pending actions
        // check resends, ACKs and done peers
        while ((peer = _activePeers.peek()) != null && (nextSendTime = peer.getNextSendTime()) <= now) {
            if (!peer.lock()) {
                requeuePeer(peer, now); // see you again
                continue;
            }
            try {
                // resend or retry after no b/w: 1% case
                int stateCount;
                if ((stateCount = peer.allocateSend(_states, now)) > 0 || peer.hasACKs()) { // we have something to send and we will be returning it
                    requeuePeer(peer, now); // see you again
                    preparePackets(stateCount, peer);
                    return;
                }
                if (peer.finishMessages(now)) {
                    _activePeers.poll();
                    continue;
                }
                requeuePeer(peer, now); // see you again
            } finally { peer.unlock(); }
        }
        // Step 3 : Copy queued ACKs
        while ((peer = _peersToACK.poll()) != null)
            queuePeer(peer, now);
        // Step 4 : sleep
        allocateTemp();
        peer = _senders.take(Math.max(nextSendTime - now, 1));
        // back to step 3
        if (peer != null)
            sendFast(peer, _context.clock().now());
    }

    /**
     *  @return null if peer is null
     */
    private void preparePacketsFast(OutboundMessageState state, PeerState peer) {
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("Sending " + state);
        // build the list of fragments to send
        int fragments = state.getFragmentCount();
        for (int j = 0; j < fragments; j++) {

            UDPPacket pkt = _builder.buildPacketFast(state, j, peer);
            if (pkt != null) {
                if (_log.shouldDebug())
                    _log.debug("Built packet with 1 fragments totalling " + state.fragmentSize(j) +
                              " data bytes to " + peer);
            } else {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Build packet FAIL for 1 fragment to " + peer);
                continue;
            }

            // following for debugging and stats
            pkt.setFragmentCount(1);
            pkt.setMessageType(state.getMessage().getMessageTypeId());  //type of first fragment
            _transport._pusher.send(pkt);
        }

        peer.packetsTransmitted(fragments);
        if (_log.shouldDebug())
            _log.debug("Sent " + fragments + " fragments of " + state +
                      " messages in " + fragments + " packets to " + peer);
    }

    /**
     *  @return null if peer is null
     */
    private void preparePackets(int stateCount, PeerState peer) {
        if (stateCount == 0) {
            do {
                UDPPacket pkt = _builder.buildACK(peer);
                if (pkt == null) {
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Build ACK FAIL for" + peer);
                    return;
                }
                if (_log.shouldLog(Log.INFO))
                    _log.info("Built ACK for " + peer);
                _transport._pusher.send(pkt);
            } while (peer.hasACKs() && peer.countACKs() > PacketBuilder.ABSOLUTE_MAX_ACKS >> 1);
            return;
        }
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("Sending " + _states);
        // build the list of fragments to send
        List<Fragment> toSend = new ArrayList<Fragment>(8);
        OutboundMessageState state;
        for (int i = 0; i < stateCount; i++) {
            state = _states[i];
            int fragments = state.getFragmentCount();
            int pc = state.getPushCount();
            int queued = 0;
            if (fragments == 1) {
                queued = 1;
                toSend.add(new Fragment(state, 0));
            } else if (pc <= 1) {
                for (int j = 0; j < fragments; j++)
                    toSend.add(new Fragment(state, j));
            } else {
                for (int j = 0; j < fragments; j++) {
                    queued++;
                    if (state.needsSending(j))
                        toSend.add(new Fragment(state, j));
                }
            }
            // per-state stats
            if (pc > 1) {
                peer.messageRetransmitted(queued);
                // _packetsRetransmitted += toSend; // lifetime for the transport
                _context.statManager().addRateData("udp.packetsRetransmitted", state.getLifetime(), peer.getPacketsTransmitted());
                if (_log.shouldLog(Log.INFO))
                    _log.info("Retransmitting " + state + " to " + peer);
            }
        }

//        if (toSend.isEmpty())
//            return; // can not happen

        int fragmentsToSend = toSend.size();
        // sort by size, biggest first
        // don't bother unless more than one state (fragments are already sorted within a state)
        // This puts the DeliveryStatusMessage after the DatabaseStoreMessage, don't do it for now.
        // It also undoes the ordering of the priority queue in PeerState.
        //if (fragmentsToSend > 1 && states.size() > 1)
        //    Collections.sort(toSend, new FragmentComparator());

        int sent = 0;
        for (int i = 0; i < toSend.size(); i++) {
            int nextCount = 1;
            Fragment next = toSend.get(i);
            _fragments[0] = next;
            state = next.state;
            OutNetMessage msg = state.getMessage();
            int msgType = (msg != null) ? msg.getMessageTypeId() : -1;
            if (_log.shouldDebug())
                _log.debug("Building packet for " + next + " to " + peer);
            int curTotalDataSize = state.fragmentSize(next.num);
            // now stuff in more fragments if they fit
            if (i + 1 < toSend.size()) {
                int maxAvail = PacketBuilder.getMaxAdditionalFragmentSize(peer, nextCount, curTotalDataSize);
                for (int j = i + 1; j < toSend.size(); j++) {
                    next = toSend.get(j);
                    int nextDataSize = next.state.fragmentSize(next.num);
                    //if (PacketBuilder.canFitAnotherFragment(peer, sendNext.size(), curTotalDataSize, nextDataSize)) {
                    //if (_builder.canFitAnotherFragment(peer, sendNext.size(), curTotalDataSize, nextDataSize)) {
                    if (nextDataSize <= maxAvail) {
                        // add it
                        toSend.remove(j);
                        j--;
                        _fragments[nextCount++] = next;
                        curTotalDataSize += nextDataSize;
                        maxAvail = PacketBuilder.getMaxAdditionalFragmentSize(peer, nextCount, curTotalDataSize);
                        if (_log.shouldLog(Log.INFO))
                            _log.info("Adding in additional " + next + " to " + peer);
                    }  // else too big
                }
            }

            UDPPacket pkt = _builder.buildPacket(_fragments, nextCount, peer);
            if (pkt != null) {
                if (_log.shouldDebug())
                    _log.debug("Built packet with " + nextCount + " fragments totalling " + curTotalDataSize +
                              " data bytes to " + peer);
            } else {
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Build packet FAIL for " + _fragments + " to " + peer);
                continue;
            }

            // following for debugging and stats
            pkt.setFragmentCount(nextCount);
            pkt.setMessageType(msgType);  //type of first fragment
            _transport._pusher.send(pkt);
            sent++;
        }

        peer.packetsTransmitted(sent);
        if (_log.shouldDebug())
            _log.debug("Sent " + fragmentsToSend + " fragments of " + stateCount +
                      " messages in " + sent + " packets to " + peer);
    }

    /**
     *  Biggest first
     *  @since 0.9.16
     */
/****
    private static class FragmentComparator implements Comparator<Fragment>, Serializable {

        public int compare(Fragment l, Fragment r) {
            // reverse
            return r.state.fragmentSize(r.num) - l.state.fragmentSize(l.num);
        }
    }
****/

    /** throttle */
    public interface ActiveThrottle {
        public void choke(Hash peer);
        public void unchoke(Hash peer);
        public boolean isChoked(Hash peer);
    }
}
