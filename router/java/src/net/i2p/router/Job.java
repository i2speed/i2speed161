package net.i2p.router;
/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain 
 * with no warranty of any kind, either expressed or implied.  
 * It probably won't make your computer catch on fire, or eat 
 * your children, but it might.  Use at your own risk.
 *
 */


/**
 * Defines an executable task
 *
 * For use by the router only. Not to be used by applications or plugins.
 */
public interface Job {
    /**
     * Descriptive name of the task
     */
    public String getName();
    /** unique id */
    public long getJobId();

    /**
     * Actually perform the task.  This call blocks until the Job is complete.
     */
    public void runJob();
    
    /**
     * # of milliseconds after the epoch to start the job
     *
     */
    public long getStartAfter();

    /**
     * WARNING - this does not force a resort of the job queue any more...
     * ALWAYS call JobImpl.requeue() instead if job is already queued.
     */
    public void setStartAfter(long startTime);
    public void forceStartAfter(long startTime);

    public void updateStart();

    public boolean isUpdated();

    /**
     * # of milliseconds after the epoch the job actually started
     *
     */
    public long getActualStart();
    public void setActualStart(long actualStartTime);
    /**
     * Notify the timing that the job began
     *
     */
    public void start(long now);

    /**
     * # of milliseconds after the epoch the job actually ended
     *
     */
    public long getActualEnd();
    public void setActualEnd(long actualEndTime);

    /**
     * Notify the timing that the job finished
     *
     */
    public void end(long now);

    public void offsetChanged(long delta);

    /**
     *  @deprecated
     *  @return null always
     */
    @Deprecated
    public Exception getAddedBy();
    
    /** 
     * the router is extremely overloaded, so this job has been dropped.  if for
     * some reason the job *must* do some cleanup / requeueing of other tasks, it
     * should do so here.
     *
     */
    public void dropped();
}
