package net.i2p.router;
/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain 
 * with no warranty of any kind, either expressed or implied.  
 * It probably won't make your computer catch on fire, or eat 
 * your children, but it might.  Use at your own risk.
 *
 */

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

import net.i2p.router.message.HandleGarlicMessageJob;
import net.i2p.router.networkdb.kademlia.HandleFloodfillDatabaseLookupMessageJob;
import net.i2p.router.networkdb.kademlia.IterativeSearchJob;
import net.i2p.router.util.CachedIteratorCollection;
import net.i2p.stat.StatManager;
import net.i2p.util.Clock;
import net.i2p.util.I2PThread;
import net.i2p.util.LockFreeQueue;
import net.i2p.util.Log;
import net.i2p.util.CopyOnWriteMap;

import java.lang.management.*;


/**
 * Manage the pending jobs according to whatever algorithm is appropriate, giving
 * preference to earlier scheduled jobs.
 *
 * For use by the router only. Not to be used by applications or plugins.
 */
public class JobQueue {
    private final Log _log;
    private final RouterContext _context;
    
    private static int DEFAULT_RUNNERS = 3;
    private static int MAX_RUNNERS = 5;
    /** Integer (runnerId) to JobQueueRunner for created runners */
    private final JobQueueRunner[] _runners = new JobQueueRunner[MAX_RUNNERS];
    /** list of jobs that are ready to run with priority */
    private final LockFreeQueue<Job> _prioJobs;
    /** Jobs that need their stats updated */
    private final LockFreeQueue<Job> _finishedJobs;
    /** list of jobs that are ready to run ASAP */
    private final CachedIteratorCollection<Job> _readyJobs;
    /** SortedSet of jobs that are scheduled for running in the future, earliest first */
    private final TreeSet<Job> _timedJobs;
    /** job name to JobStat for that job */
    private final CopyOnWriteMap<String, JobStats> _jobStats;
    private final QueuePumper _pumper;
    /** will we allow the # job runners to grow beyond 1? */
    private volatile boolean _allowParallelOperation;
    /** have we been killed or are we alive? */
    private boolean _alive;
    
    private final Object _jobLock;
    private final AtomicInteger _activeRunners = new AtomicInteger();
    
    /** router.config parameter to override the max runners */
//    private final static String PROP_MAX_RUNNERS = "router.maxJobRunners";
    
    
    /** if a job is this lagged, spit out a warning, but keep going */
    private int _lagWarning = DEFAULT_LAG_WARNING;
    private final static int DEFAULT_LAG_WARNING = 5*1000;
    /** @deprecated unimplemented */
    @Deprecated
    private final static String PROP_LAG_WARNING = "router.jobLagWarning";
    
    /** if a job is this lagged, the router is hosed, so spit out a warning (dont shut it down) */
    private int _lagFatal = DEFAULT_LAG_FATAL;
    private final static int DEFAULT_LAG_FATAL = 30*1000;
    /** @deprecated unimplemented */
    @Deprecated
    private final static String PROP_LAG_FATAL = "router.jobLagFatal";
    
    /** if a job takes this long to run, spit out a warning, but keep going */
    private int _runWarning = DEFAULT_RUN_WARNING;
    private final static int DEFAULT_RUN_WARNING = 5*1000;
    /** @deprecated unimplemented */
    @Deprecated
    private final static String PROP_RUN_WARNING = "router.jobRunWarning";
    
    /** if a job takes this long to run, the router is hosed, so spit out a warning (dont shut it down) */
    private int _runFatal = DEFAULT_RUN_FATAL;
    private final static int DEFAULT_RUN_FATAL = 30*1000;
    /** @deprecated unimplemented */
    @Deprecated
    private final static String PROP_RUN_FATAL = "router.jobRunFatal";
    
    /** don't enforce fatal limits until the router has been up for this long */
    private int _warmupTime = DEFAULT_WARMUP_TIME;
    private final static int DEFAULT_WARMUP_TIME = 10*60*1000;
    /** @deprecated unimplemented */
    @Deprecated
    private final static String PROP_WARMUP_TIME = "router.jobWarmupTime";
    
    /** max ready and waiting jobs before we start dropping 'em */
    private int _maxWaitingJobs = DEFAULT_MAX_WAITING_JOBS;
    private final static int DEFAULT_MAX_WAITING_JOBS = 200;
    private final static int MIN_LAG_TO_DROP = 1000;

    /** @deprecated unimplemented */
    @Deprecated
    private final static String PROP_MAX_WAITING_JOBS = "router.maxWaitingJobs";

    /** 
     * queue runners wait on this whenever they're not doing anything, and 
     * this gets notified *once* whenever there are ready jobs
     */
    private final Object _runnerLock = new Object();

    /** 
     *  Does not start the pumper. Caller MUST call startup.
     */
    public JobQueue(RouterContext context) {
        _context = context;
        _log = context.logManager().getLog(JobQueue.class);
        _context.statManager().createRateStat("jobQueue.readyJobs", 
                                              "How many ready and waiting jobs there are?", 
                                              "JobQueue", 
                                              new long[] { 60*1000l, 60*60*1000l, 24*60*60*1000l });
        _context.statManager().createRateStat("jobQueue.droppedJobs", 
                                              "How many jobs do we drop due to insane overload?", 
                                              "JobQueue", 
                                              new long[] { 60*1000l, 60*60*1000l, 24*60*60*1000l });
        _context.statManager().createRateStat("jobQueue.queuedJobs",
                                              "How many scheduled jobs are there?",
                                              "JobQueue",
                                              new long[] { 60*1000l, 60*60*1000l, 24*60*60*1000l });
        // following are for JobQueueRunner
        _context.statManager().createRateStat("jobQueue.jobRun", "How long jobs take", "JobQueue", new long[] { 60*60*1000l, 24*60*60*1000l });
        _context.statManager().createRateStat("jobQueue.jobRunSlow", "How long jobs that take over a second take", "JobQueue", new long[] { 60*60*1000l, 24*60*60*1000l });
        _context.statManager().createRequiredRateStat("jobQueue.jobLag", "Job run delay (ms)", "JobQueue", new long[] { 60*1000l, 60*60*1000l, 24*60*60*1000l });

        _readyJobs = new CachedIteratorCollection<Job>();
        _prioJobs = new LockFreeQueue<Job>(999);
        _finishedJobs = new LockFreeQueue<Job>(256);
        _timedJobs = new TreeSet<Job>(new JobComparator());
        _jobLock = new Object();
        _jobStats = new CopyOnWriteMap<String,JobStats>(256);
        _pumper = new QueuePumper();
    }

    /**
     */
    private void queueJob(Job job, boolean priority) {
        if (!_alive) return;
        if (priority) {
            priority = _prioJobs.offer(job);
            if (priority && _activeRunners.get() == MAX_RUNNERS)
                return;
        }
        synchronized (_readyJobs) {
            if (priority) {
                _readyJobs.notify();
                return;
            }
            _readyJobs.add(job);
            if (_activeRunners.get() < DEFAULT_RUNNERS) // free runner exists
                _readyJobs.notify();
        }
    }
    
    /**
     * Enqueue the specified prio job
     */
    public void prioJob(Job job) {
        if (job == null) return;
        // don't skew us - its 'start after' its been queued, or later
        job.forceStartAfter(_context.clock().now());
        queueJob(job, true);
    }

    /**
     * Enqueue the specified job
     */
    public void addJob(Job job) {
        if (job == null) return;

        // getNext() is now outside the jobLock, is that ok?
        if (job.isUpdated()) {
            synchronized (_timedJobs) {
                if (_timedJobs.remove(job)) {
                    if (_log.shouldLog(Log.WARN))
                    _log.warn("Rescheduling job: " + job);
                }
            }
            job.updateStart();
        }
        long start = job.getStartAfter();
        long now = _context.clock().now();
        if (start <= now) {
            // don't skew us - its 'start after' its been queued, or later
            job.forceStartAfter(now);
            queueJob(job, false);
        } else {
            int numReady = _readyJobs.size();
            boolean dropped = false;
                if (start > now + 3*24*60*60*1000L) {
                // catch bugs, Job.requeue() argument is a delay not a time
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Scheduling job far in the future: " + (new Date(start)) + ' ' + job);
            }
            // Always remove and re-add, since it needs to be
            // re-sorted in the TreeSet.
            if (shouldDrop(job, numReady)) {
                job.dropped();
                dropped = true;
            } else synchronized (_timedJobs) {
                    _timedJobs.add(job);
                // we do not notify, the runner is only a precaution
            }
            _context.statManager().addRateData("jobQueue.readyJobs", numReady);
            _context.statManager().addRateData("jobQueue.queuedJobs", _timedJobs.size());
            if (dropped) {
                _context.statManager().addRateData("jobQueue.droppedJobs", 1);
                if (_log.shouldLog(Log.WARN))
                    _log.warn("Dropping job due to overload!  # ready jobs: " 
                                + numReady + ": job = " + job);
                String key = job.getName();
                JobStats stats = _jobStats.get(key);
                if (stats == null) {
                    stats = new JobStats(key);
                    JobStats old = _jobStats.putIfAbsent(key, stats);
                    if (old != null)
                        stats = old;
                }
                stats.jobDropped();
            }
        }
        
    }
    
    public void removeJob(Job job) {
        synchronized (_timedJobs) {
            _timedJobs.remove(job);
        }
    }
    
    /**
     * Returns <code>true</code> if a given job is waiting for execution or running;
     * <code>false</code> if the job is finished or doesn't exist in the queue.
     */
    // private boolean isJobActive(Job job) {
    //     return _readyJobs.contains(job) || _prioJobs.contains(job);
    // }
    
    /**
     *  @deprecated contention - see JobTiming.setStartAfter() comments
     */
    @Deprecated
    public void timingUpdated() {
        synchronized (_jobLock) {
            _jobLock.notifyAll();
        }
    }
    
    public int getReadyCount() { 
            return _readyJobs.size() + _prioJobs.size();
    }

    int _lag;
    public int getMaxLag() { 
        return _lag;
    }
    
    /** 
     * are we so overloaded that we should drop the given job?  
     * This is driven both by the numReady and waiting jobs, the type of job
     * in question, and what the router's router.maxWaitingJobs config parameter 
     * is set to.
     *
     */
    private boolean shouldDrop(Job job, long numReady) {
        if (_maxWaitingJobs <= 0) return false; // dont ever drop jobs
        if (!_allowParallelOperation) return false; // dont drop during startup [duh]
        if (numReady > _maxWaitingJobs) {
            Class<? extends Job> cls = job.getClass();
            // lets not try to drop too many tunnel messages...
            //if (cls == HandleTunnelMessageJob.class)
            //    return true;
                
            // we don't really *need* to answer DB lookup messages
            // This is pretty lame, there's actually a ton of different jobs we
            // could drop, but is it worth making a list?
            //
            // Garlic added in 0.9.19, floodfills were getting overloaded
            // with encrypted lookups
            //
            // ISJ added in 0.9.31, can get backed up due to DNS
            //
            // Obviously we can only drop one-shot jobs, not those that requeue
            //
            if (cls == HandleFloodfillDatabaseLookupMessageJob.class ||
                cls == HandleGarlicMessageJob.class ||
                cls == IterativeSearchJob.class) {
                // this tail drops based on the lag at the tail, which
                // makes no sense...
                //JobTiming jt = job.getTiming();
                //if (jt != null) {
                //    long lag =  _context.clock().now() - jt.getStartAfter();
                //    if (lag >= MIN_LAG_TO_DROP)
                //        return true;
                //}

                // this tail drops based on the lag at the head
                if (getMaxLag() >= MIN_LAG_TO_DROP)
                    return true;
            }
        }
        return false;
    }
    
    public void allowParallelOperation() { 
        _allowParallelOperation = true; 
//        runQueue(_context.getProperty(PROP_MAX_RUNNERS, RUNNERS));
    }
    
    /** 
     *  Start the pumper.
     *  @since 0.9.19
     */
    public void startup() {
        _alive = true;
        I2PThread pumperThread = new I2PThread(_pumper, "Job Queue Pumper", true);
        pumperThread.setPriority(Thread.NORM_PRIORITY + 1);
        pumperThread.start();
    }

    /** @deprecated do you really want to do this? */
    @Deprecated
    public void restart() {
        synchronized (_readyJobs) {
            _readyJobs.clear();
        }
        _prioJobs.clear();
        synchronized (_timedJobs) {
            _timedJobs.clear();
        }
        synchronized (_jobLock) {
            _jobLock.notify();
        }
    }
    
    void shutdown() {
        _alive = false;
        restart();
        // The JobQueueRunners are NOT daemons,
        // so they must be stopped.
        for (int i = 0; i < MAX_RUNNERS; i++)
            _runners[i].interrupt();
        _finishedJobs.clear();;
        _jobStats.clear();

      /********
        if (_log.shouldLog(Log.WARN)) {
            StringBuilder buf = new StringBuilder(1024);
            buf.append("current jobs: \n");
            for (Iterator iter = _queueRunners.values().iterator(); iter.hasNext(); ) {
                JobQueueRunner runner = iter.next();
                Job j = runner.getCurrentJob();

                buf.append("Runner ").append(runner.getRunnerId()).append(": ");
                if (j == null) {
                    buf.append("no current job ");
                } else {
                    buf.append(j.toString());
                    buf.append(" started ").append(_context.clock().now() - j.getTiming().getActualStart());
                    buf.append("ms ago");
                }

                j = runner.getLastJob();
                if (j == null) {
                    buf.append("no last job");
                } else {
                    buf.append(j.toString());
                    buf.append(" started ").append(_context.clock().now() - j.getTiming().getActualStart());
                    buf.append("ms ago and finished ");
                    buf.append(_context.clock().now() - j.getTiming().getActualEnd());
                    buf.append("ms ago");
                }
            }
            buf.append("\nready jobs: ").append(_readyJobs.size()).append("\n\t");
            for (int i = 0; i < _readyJobs.size(); i++) 
                buf.append(_readyJobs.get(i).toString()).append("\n\t");
            buf.append("\n\ntimed jobs: ").append(_timedJobs.size()).append("\n\t");
            for (int i = 0; i < _timedJobs.size(); i++) 
                buf.append(_timedJobs.get(i).toString()).append("\n\t");
            _log.log(Log.WARN, buf.toString());
        }
      ********/
    }

    boolean isAlive() { return _alive; }
    
    /**
     * When did the most recently begin job start?
     *
     * @since Broken before 0.9.51, always returned -1
     */
    public long getLastJobBegin() { 
        long when = -1;
        for (int i = 0; i < MAX_RUNNERS; i++) {
            long cur = _runners[i].getLastBegin();
            if (cur > when)
                when = cur;
        }
        return when; 
    }
    /**
     * When did the most recently begin job start?
     */
    public long getLastJobEnd() { 
        long when = -1;
        for (int i = 0; i < MAX_RUNNERS; i++) {
            long cur = _runners[i].getLastEnd();
            if (cur > when)
                when = cur;
        }
        return when; 
    }
    /** 
     * retrieve the most recently begin and still currently active job, or null if
     * no jobs are running
     */
    public Job getLastJob() { 
        Job j = null;
        long when = -1;
        for (int i = 0; i < MAX_RUNNERS; i++) {
            JobQueueRunner cur = _runners[i];
            if (cur.getLastBegin() > when) {
                j = cur.getCurrentJob();
                when = cur.getLastBegin();
            }
        }
        return j;
    }
    
    /**
     * Blocking call to retrieve the next ready job
     *
     */
    Job getNext(int id) {
        Job last = _runners[id].getLastJob();
        boolean restart = last != null;
        if (restart)
            _finishedJobs.offer(last);
        try {
            while (_alive) {
                long now = _context.clock().now();
                Job j = _prioJobs.threaded_poll(_readyJobs.size() == 0);
                if (j != null) {
                    _lag = _lag + (int)(now - j.getStartAfter()) >> 1;
                    return j;
                }
                int active = _activeRunners.get() - (restart ? 1 : 0);
                if (active < DEFAULT_RUNNERS) {
                    synchronized (_readyJobs) {
                        j = _readyJobs.poll();
                    }
                    if (j != null) {
                        _lag = _lag + (int)(now - j.getStartAfter()) >> 1;
                        return j;
                    }
                    synchronized (_timedJobs) {
                        if (_timedJobs.size() > 0 && _timedJobs.first().getStartAfter() <= now) {
                            j = _timedJobs.pollFirst();
                            return j;
                        }
                    }
                }
                if (restart) _activeRunners.decrementAndGet();
                synchronized (_readyJobs) {
                    try {
                        _readyJobs.wait(50);
                    } catch (InterruptedException ie) {}
                }
                restart = false;
            }
        } finally {
            if (!restart) _activeRunners.incrementAndGet();
        }
        if (_log.shouldLog(Log.WARN))
            _log.warn("No longer alive, returning null");
        return null;
    }
    
    /**
     * Start up the queue with the specified number of concurrent processors.
     * If this method has already been called, it will adjust the number of 
     * runners if necessary.  This does not ever stop or reduce threads.
     */
    public synchronized void runQueue(int numThreads) {
        // we're still starting up [serially] and we've got at least one runner
        // or no change specified,
            // so dont do anything
        int rid = 0;
        while (rid < MAX_RUNNERS) {
            JobQueueRunner runner = new JobQueueRunner(_context, rid);
            _runners[rid] = runner;
            runner.setName("JobQueue " + ++rid);
            runner.start();
            try {
                Thread.sleep(5);
            } catch (InterruptedException ignored) {
            }
        }

        // we've already enabled parallel operation, so adjust to however many are
            // specified
//            if (_log.shouldLog(Log.INFO))
//            _log.info("Changing the number of queue runners from "
//                            + _numRunners + " to " + numThreads);
    }
    
    /**
     * Responsible for moving jobs from the timed queue to the ready queue, 
     * adjusting the number of queue runners, as well as periodically updating the 
     * max number of runners.
     *
     */
    private final class QueuePumper implements Runnable, Clock.ClockUpdateListener, RouterClock.ClockShiftListener {
        public QueuePumper() { 
            _context.clock().addUpdateListener(this);
            ((RouterClock) _context.clock()).addShiftListener(this);
        }

        public void GenerateThreadDump() {
            try {
                ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
                ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
                for (ThreadInfo threadInfo : threadInfos) {
                    if (threadInfo.getThreadName().contains("JobQ")) {
                        Thread.State state = threadInfo.getThreadState();
                        System.out.println(threadInfo.getThreadName() + ": " + state);
                        StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
                        for (StackTraceElement stackTraceElement : stackTraceElements)
                            System.out.println("        at " + stackTraceElement);
                        System.out.println();
                    }
                }
            } catch (NullPointerException npe) {}
        }

        public void run() {
            StatManager stat = _context.statManager();
            long lastdump = 0;
            try {
                while (_alive) {
                    long now = _context.clock().now();
                    if (_prioJobs.size() > 50)
                    synchronized (_readyJobs) {
                        if (_readyJobs.size() > 50 && now - lastdump > 60*1000) {
                        lastdump = now;
                        GenerateThreadDump();
                       }
                    }
                    Job j;
                    int totalLag = 0;
                    int totalDuration = 0;
                    int numJobs = 0;
                    while ((j = _finishedJobs.poll()) != null) {
                        long start = j.getActualStart();
                        int duration = (int)(j.getActualEnd() - start);
                        int lag = (int)(start - j.getStartAfter());
                        if (lag < 0) lag = 0;

                        updateStats(j, lag, duration);
                        totalLag += lag;
                        totalDuration += duration;
                        numJobs++;
                    }
                    stat.addRateDataCount("jobQueue.jobRun", totalDuration, totalDuration, numJobs);
                    stat.addRateDataCount("jobQueue.jobLag", totalLag, numJobs);

                    //if (_log.shouldLog(Log.DEBUG))
                    //    _log.debug("Waiting " + timeToWait + " before rechecking the timed queue");
                    try {
                        synchronized(_jobLock) {
                            _jobLock.wait(100);
                        }
                    } catch (InterruptedException ie) {}
                } // while (_alive)
            } catch (Throwable t) {
                if (_log.shouldLog(Log.ERROR))
                    _log.error("pumper killed?!", t);
            } finally {
                _context.clock().removeUpdateListener(this);
                ((RouterClock) _context.clock()).removeShiftListener(this);
            }
        }

        public void offsetChanged(long delta) {
            updateJobTimings(delta);
            synchronized (_jobLock) {
                _jobLock.notifyAll();
            }
        }

        /**
         *  Clock shift listener.
         *  Only adjust timings for negative shifts.
         *  For positive shifts, just wake up the pumper.
         *  @since 0.9.23
         */
        public void clockShift(long delta) {
            if (delta < 0) {
                offsetChanged(delta);
            } else {
                synchronized (_jobLock) {
                    _jobLock.notifyAll();
                }
            }
        }

    }
    
    /**
     * Update the clock data for all jobs in process or scheduled for
     * completion.
     */
    private void updateJobTimings(long delta) {
        synchronized (_timedJobs) {
            for (Job j : _timedJobs) {
                j.offsetChanged(delta);
            }
        }
        // skip other jobs for now;
    }
    
    /**
     * calculate and update the job timings
     * if it was lagged too much or took too long to run, spit out
     * a warning (and if its really excessive, kill the router)
     */
    void updateStats(Job job, int lag, int duration) {
        if (_context.router() == null) return;
        String key = job.getName();
        JobStats stats = _jobStats.get(key);
        if (stats == null) {
            stats = new JobStats(key);
            _jobStats.put(key, stats);
        }
        if (duration < 0) duration = 0;
        stats.jobRan(duration, lag);
        if (lag <= _lagWarning && duration <= _runWarning)
            return;

        String dieMsg = null;

        if (lag > _lagWarning) {
            dieMsg = "Lag too long for job " + job.getName() + " [" + lag + "ms and a run time of " + duration + "ms]";
        } else {
            dieMsg = "Job run too long for job " + job.getName() + " [" + lag + "ms lag and run time of " + duration + "ms]";
        }

            if (_log.shouldLog(Log.WARN))
                _log.warn(dieMsg);
        MessageHistory hist = _context.messageHistory();
            if (hist != null)
                hist.messageProcessingError(-1, JobQueue.class.getName(), dieMsg);

        if (_context.router().getUptime() <= _warmupTime)
            return;

        if (lag > _lagFatal) {
            // this is fscking bad - the network at this size shouldn't have this much real contention
            // so we're going to DIE DIE DIE
            if (_log.shouldLog(Log.WARN))
                _log.log(Log.WARN, "The router is either incredibly overloaded or (more likely) there's an error.", new Exception("ttttooooo mmmuuuccccchhhh llllaaagggg"));
            //try { Thread.sleep(5000); } catch (InterruptedException ie) {}
            //Router.getInstance().shutdown();
            return;
        }
        
        if (duration > _runFatal) {
            // slow CPUs can get hosed with ElGamal, but 10s is too much.
            if (_log.shouldLog(Log.WARN))
                _log.log(Log.WARN, "The router is incredibly overloaded - either you have a 386, or (more likely) there's an error. ", new Exception("ttttooooo sssllloooowww"));
            //try { Thread.sleep(5000); } catch (InterruptedException ie) {}
            //Router.getInstance().shutdown();
        }
    }

    /**
     *  Comparator for the _timedJobs TreeSet.
     *  Ensure different jobs with the same timing are different so they aren't removed.
     *  @since 0.8.9
     */
    private static class JobComparator implements Comparator<Job>, Serializable {
         public int compare(Job l, Job r) {
             // equals first, Jobs generally don't override so this should be fast
             // And this MUST be first so we can remove a job even if its timing has changed.
             if (l.equals(r))
                 return 0;
             // This is for _timedJobs, which always have a JobTiming.
             long ld = l.getStartAfter() - r.getStartAfter();
             if (ld < 0)
                 return -1;
             if (ld > 0)
                 return 1;
             ld = l.getJobId() - r.getJobId();
             if (ld < 0)
                 return -1;
             if (ld > 0)
                 return 1;
             return l.hashCode() - r.hashCode();
        }
    }

    public int countJobs() {
        return _activeRunners.get();
    }
    
    /**
     *  Dump the current state.
     *  For the router console jobs status page.
     *
     *  @param readyJobs out parameter
     *  @param timedJobs out parameter
     *  @param activeJobs out parameter
     *  @param justFinishedJobs out parameter
     *  @return number of job runners
     *  @since 0.8.9
     */
    public int getJobs(Collection<Job> readyJobs, Collection<Job> timedJobs,
                       Collection<Job> activeJobs, Collection<Job> justFinishedJobs) {
        for (int i = 0; i < MAX_RUNNERS; i++) {
            JobQueueRunner r = _runners[i];
            Job job = r.getCurrentJob();
            if (job != null) {
                activeJobs.add(job);
            } else {
                job = r.getLastJob();
                if (job != null)
                    justFinishedJobs.add(job);
            }
        }
        _prioJobs.populate(readyJobs);
        synchronized (_readyJobs) {
            readyJobs.addAll(_readyJobs);
        }
        synchronized (_timedJobs) {
            timedJobs.addAll(_timedJobs);
        }
        return MAX_RUNNERS;
    }

    /**
     *  Current job stats.
     *  For the router console jobs status page.
     *
     *  @since 0.8.9
     */
    public Collection<JobStats> getJobStats() {
        return Collections.unmodifiableCollection(_jobStats.values());
    }

    /** @deprecated moved to router console */
    @Deprecated
    public void renderStatusHTML(Writer out) throws IOException {
    }
}
