/*
 * Copyright (C) 2016 Southern Storm Software, Pty Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.southernstorm.noise.crypto;

import java.util.Arrays;

import com.southernstorm.noise.protocol.Destroyable;

/**
 * Simple implementation of the Poly1305 message authenticator.
 */
public final class Poly1305 implements Destroyable, Cloneable {

	// The 130-bit intermediate values are broken up into five 26-bit words.
	private final byte[] nonce;
	private final byte[] block;
	private int posn, r0, r1, r2, r3, r4, h0, h1, h2, h3, h4;

	/**
	 * Constructs a new Poly1305 message authenticator.
	 */
	public Poly1305()
	{
		nonce = new byte [16];
		block = new byte [16];
		posn = 0;
	}

	/**
	 * Resets the message authenticator with a new key.
	 * 
	 * @param key The buffer containing the 32 byte key.
	 * @param offset The offset into the buffer of the first key byte.
	 */
	public void reset(byte[] key, int offset)
	{
		System.arraycopy(key, offset + 16, nonce, 0, 16);
		h0 = h1 = h2 = h3 = h4 = posn = 0;
		
		// Convert the first 16 bytes of the key into a 130-bit
		// "r" value while masking off the bits that we don't need.
		int k = key[offset + 3];
		int k6 = key[offset + 6];
		r0 = ((key[offset] & 0xFF)) |
			   ((key[offset + 1] & 0xFF) << 8) |
			   ((key[offset + 2] & 0xFF) << 16) |
			   ((k & 0x03) << 24);
		r1 = ((k & 0x0C) >> 2) |
			   ((key[offset + 4] & 0xFC) << 6) |
			   ((key[offset + 5] & 0xFF) << 14) |
			   ((k6 & 0x0F) << 22);
		k = key[offset + 9];
		r2 = ((k6 & 0xF0) >> 4) |
			   ((key[offset + 7] & 0x0F) << 4) |
			   ((key[offset + 8] & 0xFC) << 12) |
			   ((k & 0x3F) << 20);
		r3 = ((k & 0xC0) >> 6) |
			   ((key[offset + 10] & 0xFF) << 2) |
			   ((key[offset + 11] & 0x0F) << 10) |
			   ((key[offset + 12] & 0xFC) << 18);
		r4 = ((key[offset + 13] & 0xFF)) |
			   ((key[offset + 14] & 0xFF) << 8) |
			   ((key[offset + 15] & 0x0F) << 16);
	}

	/**
	 * Updates the message authenticator with more input data.
	 * 
	 * @param data The buffer containing the input data.
	 * @param offset The offset of the first byte of input.
	 * @param length The number of bytes of input.
	 */
	public void update(byte[] data, int offset, int length) {
		if (posn != 0) {
			int temp = 16 - posn;
			if (length < temp) {
				// Collect up partial bytes in the block buffer.
				System.arraycopy(data, offset, block, posn, length);
				posn += length;
				return;
			}
			System.arraycopy(data, offset, block, posn, temp);
			offset += temp;
			length -= temp;
			processChunk(block, 0, false);
		};
		for (int i = length >> 4; i > 0; i--) {
			// We can process the chunk directly out of the input buffer.
			processChunk(data, offset, false);
			offset += 16;
		}
		posn = length & 0xf;
		if (posn > 0)
			System.arraycopy(data, offset, block, 0, posn);
	}

	/**
	 * Pads the input with zeroes to a multiple of 16 bytes.
	 */
	public void pad()
	{
		if (posn != 0) {
			Arrays.fill(block, posn, 16, (byte)0);
			processChunk(block, 0, false);
			posn = 0;
		}
	}

	/**
	 * Finishes the message authenticator and returns the 16-byte token.
	 * 
	 * @param token The buffer to receive the token.
	 * @param offset The offset of the token in the buffer.
	 */
	public void finish(byte[] token, int offset)
	{
		// Pad and flush the final chunk.
		if (posn != 0) {
			block[posn] = (byte)1;
			Arrays.fill(block, posn + 1, 16, (byte)0);
			processChunk(block, 0, true);
		}
		
	    // At this point, processChunk() has left h as a partially reduced
	    // result that is less than (2^130 - 5) * 6.  Perform one more
	    // reduction and a trial subtraction to produce the final result.

	    // Multiply the high bits of h by 5 and add them to the 130 low bits.
		int carry = (h4 >> 26) * 5 + h0;
		h0 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h1;
		h1 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h2;
		h2 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h3;
		h3 = carry & 0x03FFFFFF;
		h4 = (carry >> 26) + (h4 & 0x03FFFFFF);

	    // Subtract (2^130 - 5) from h by computing c = h + 5 - 2^130.
	    // The "minus 2^130" step is implicit.
		carry = 5 + h0;
		int c0 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h1;
		int c1 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h2;
		int c2 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h3;
		int c4 = (carry >> 26) + h4;

	    // Borrow occurs if bit 2^130 of the previous c result is zero.
	    // Carefully turn this into a selection mask so we can select either
	    // h or c as the final result.
		int mask = -((c4 >> 26) & 0x01);
		int nmask = ~mask;
		h0 = (h0 & nmask) | (c0 & mask);
		h1 = (h1 & nmask) | (c1 & mask);
		h2 = (h2 & nmask) | (c2 & mask);
		h3 = (h3 & nmask) | (carry & 0x03FFFFFF & mask);
		h4 = (h4 & nmask) | (c4 & mask);
		
		// Convert h into little-endian in the block buffer.
		block[0] = (byte)(h0);
		block[1] = (byte)(h0 >> 8);
		block[2] = (byte)(h0 >> 16);
		block[3] = (byte)((h0 >> 24) | (h1 << 2));
		block[4] = (byte)(h1 >> 6);
		block[5] = (byte)(h1 >> 14);
		block[6] = (byte)((h1 >> 22) | (h2 << 4));
		block[7] = (byte)(h2 >> 4);
		block[8] = (byte)(h2 >> 12);
		block[9] = (byte)((h2 >> 20) | (h3 << 6));
		block[10] = (byte)(h3 >> 2);
		block[11] = (byte)(h3 >> 10);
		block[12] = (byte)(h3 >> 18);
		block[13] = (byte)(h4);
		block[14] = (byte)(h4 >> 8);
		block[15] = (byte)(h4 >> 16);
		
		// Add the nonce and write the final result to the token.
		carry = 0;
		for (int x = 0; x < 16; ++x) {
			carry = (carry >> 8) + (nonce[x] & 0xFF) + (block[x] & 0xFF);
			token[offset + x] = (byte)carry;
		}
	}

	/**
	 * Processes the next chunk of input data.
	 * 
	 * @param chunk Buffer containing the input data chunk.
	 * @param offset Offset of the first byte of the 16-byte chunk.
	 * @param finalChunk Set to true if this is the final chunk.
	 */
	private void processChunk(byte[] chunk, int offset, boolean finalChunk)
	{
		// Unpack the 128-bit chunk into a 130-bit value in "c".

		// Compute h = ((h + c) * r) mod (2^130 - 5)
		
		// Start with h += c.  We assume that h is less than (2^130 - 5) * 6
		// and that c is less than 2^129, so the result will be less than 2^133.
		h0 += chunk[offset] & 0xFF | (chunk[offset + 1] & 0xFF) << 8 |
			(chunk[offset + 2] & 0xFF) << 16 | (chunk[offset + 3] & 0x03) << 24;
		h1 += (chunk[offset + 3] & 0xFC) >> 2 | (chunk[offset + 4] & 0xFF) << 6 |
			(chunk[offset + 5] & 0xFF) << 14 | (chunk[offset + 6] & 0x0F) << 22;
		h2 += (chunk[offset + 6] & 0xF0) >> 4 | (chunk[offset + 7] & 0xFF) << 4 |
			(chunk[offset + 8] & 0xFF) << 12 | (chunk[offset + 9] & 0x3F) << 20;
		h3 += (chunk[offset + 9] & 0xC0) >> 6 | (chunk[offset + 10] & 0xFF) << 2 |
			(chunk[offset + 11] & 0xFF) << 10 | (chunk[offset + 12] & 0xFF) << 18;
		h4 += chunk[offset + 13] & 0xFF | (chunk[offset + 14] & 0xFF) << 8 |
			(chunk[offset + 15] & 0xFF) << 16 | (finalChunk ? 0 : 1 << 24);

		// Multiply h by r.  We know that r is less than 2^124 because the
	    // top 4 bits were AND-ed off by reset().  That makes h * r less
	    // than 2^257.  Which is less than the (2^130 - 6)^2 we want for
	    // the modulo reduction step that follows.  The intermediate limbs
		// are 52 bits in size, which allows us to collect up carries in the
		// extra bits of the 64 bit longs and propagate them later.
		long t0 = (long)h0 * r0;
		long t1 = (long)h0 * r1 + (long)h1 * r0;
		long t2 = (long)h0 * r2 + (long)h1 * r1 + (long)h2 * r0;
		long t3 = (long)h0 * r3 + (long)h1 * r2 + (long)h2 * r1 + (long)h3 * r0;
		long t4 = (long)h0 * r4 + (long)h1 * r3 + (long)h2 * r2 + (long)h3 * r1 + (long)h4 * r0;
		long t5  = (long)h1 * r4 + (long)h2 * r3 + (long)h3 * r2 + (long)h4 * r1 ;
		long t6  = (long)h2 * r4 + (long)h3 * r3 + (long)h4 * r2;
		long t7 = (long)h3 * r4 + (long)h4 * r3;
		long t8 = (long)h4 * r4;
		
		// Propagate carries to convert the t limbs from 52-bit back to 26-bit.
		// The low bits are placed into h and the high bits are placed into c.
		h0 = (int)t0 & 0x03FFFFFF;
		long hv = t1 + (t0 >> 26);
		h1 = (int)hv & 0x03FFFFFF;
		hv = t2 + (hv >> 26);
		h2 = (int)hv & 0x03FFFFFF;
		hv = t3 + (hv >> 26);
		h3 = (int)hv & 0x03FFFFFF;
		hv = t4 + (hv >> 26);
		h4 = (int)hv & 0x03FFFFFF;
		hv = t5 + (hv >> 26);
		int c0 = (int)hv & 0x03FFFFFF;
		hv = t6 + (hv >> 26);
		int c1 = (int)hv & 0x03FFFFFF;
		hv = t7 + (hv >> 26);
		int c2 = (int)hv & 0x03FFFFFF;
		hv = t8 + (hv >> 26);
		
		// Reduce h * r modulo (2^130 - 5) by multiplying the high 130 bits by 5
		// and adding them to the low 130 bits.  This will leave the result at
		// most 5 subtractions away from the answer we want.
		int carry = h0 + c0 * 5;
		h0 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h1 + c1 * 5;
		h1 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h2 + c2 * 5;
		h2 = carry & 0x03FFFFFF;
		carry = (carry >> 26) + h3 + ((int)(hv) & 0x03FFFFFF) * 5;
		h3 = carry & 0x03FFFFFF;
		h4 += (carry >> 26) + (int)(hv >>> 26) * 5;
	}

	@Override
	public void destroy() {
		Arrays.fill(nonce, (byte)0);
		Arrays.fill(block, (byte)0);
		h0 = h1 = h2 = h3 = h4 = r0 = r1 = r2 = r3 = r4 = 0;
	}

	/**
	 *  I2P
	 *  @since 0.9.44
	 */
	@Override
	public Poly1305 clone() throws CloneNotSupportedException {
		return (Poly1305) super.clone();
	}
}
